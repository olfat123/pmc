<?php /* Template Name: Home */ 
get_header();
global $road9;
$home_banner = $road9['home-banner']['url'];
$title = $road9['banner-title'];
$button_text = $road9['button-text'];
$button_icon = $road9['button-icon']['url'];
$button_link = $road9['button-link'];
?>
    <!-- banner --> 
<div class="banner" style="background-image: url(<?php echo $home_banner ?>);">
  <div class="overlay">
    <div class="container">
      <h1 class=" pb-4"><?php echo $title; ?></h1>
      <a href="<?php echo get_permalink(pll_get_post(255));?>" class="btn btn-primary" ><?php echo $button_text; ?><span class="pl-2"><img class="hvr-forward" src="<?php echo $button_icon; ?>"></span></a>
    </div>
  </div>
</div>

<div class="circle" data-aos="fade-up" data-aos-duration="3000"></div>

<!-- End of Banner -->
<?php 
    $first_section_title = $road9['first-section-title'];    
    $first_section_description = $road9['first-section-description'];
    $medical_service_icon = $road9['medical-service-icon']['url'];   
    $beauty_service_icon = $road9['beauty-service-icon']['url'];

    $second_section_title = $road9['second-section-title'];   
    $second_section_subtitle = $road9['second-section-subtitle'];  
    $second_section_description = $road9['second-section-description'];

    $third_section_title = $road9['third-section-title'];   
    $third_section_subtitle = $road9['third-section-subtitle'];  
    $third_section_description = $road9['third-section-description'];
    
?>
<!-- Provide -->
<section id="provide">
  <div class="container">
    <div class="row">

      
  <!-- ###### Side Text ####### -->

      <div class="col-lg-3 d-flex justify-content-center align-items-start flex-column w-100">
        <p class="h1 text-uppercase font-weight-bold"><?php echo $first_section_title; ?></p>
        <p class="small para"><?php echo $first_section_description; ?></p>
      </div>


      <div class="col-lg-9">
          <!--########## card 1 ############-->
          <div  data-aos="fade-up"    data-aos-duration="2000">
            <div class="card mb-3">
              <div class="row no-gutters">
                <div class="col-lg-6">
                  <div class="card-img text-uppercase">
                    <img src="<?php echo $medical_service_icon;?>" alt="">
                    <p class="h4 font-weight-bold"><?php echo __( 'Medical Services', 'road9' ) ; ?></p>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="card-body">
                    <div class="all-card-text p-4">
                      <?php
                       $args = array(
                                   'taxonomy' => 'medical_service_category'                                                       
                               );

                       $cats = get_categories($args);

                       if($cats){

                          foreach($cats as $cat) {
                          ?>
                                <h5 class="card-title">
                                     <?php echo $cat->name; ?>
                                </h5>
                                <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. dignissimos voluptates.</p>
                          <?php
                           }
                         }

                      ?>                
                    </div>
                    <a href="<?php echo get_term_link(pll_get_term(22));?>" class="card-link text-uppercase" onmouseover="over()" onmouseout="out()">
                      <span class="card-link-text"><?php echo __( 'Medical Services', 'road9' ) ; ?></span>
                      <span class="card-link-arrow text-uppercase">
                        <img src="<?php echo $button_icon; ?>" alt="arrow" class="px-2">
                      </span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- ####### card 2 #########-->
          <div  data-aos="fade-up"  data-aos-duration="2000">
            <div class="card mb-3">
              <div class="row no-gutters">
                <div class="col-lg-6">
                  <div class="card-img text-uppercase">
                      <img src="<?php echo $beauty_service_icon;?>" alt="">
                    <p class="h4 font-weight-bold"><?php echo __( 'Beauty Services', 'road9' ) ; ?></p>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="card-body">
                    <div class="all-card-text p-4">
                      <?php
                       $args = array(
                                   'taxonomy' => 'beauty_service_category',
                                   'orderby' => 'name',
                                   'order'   => 'ASC'
                               );

                       $cats = get_categories($args);

                       foreach($cats as $cat) {
                          ?>
                          <h5 class="card-title">
                               <?php echo $cat->name; ?>
                          </h5>
                          <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. dignissimos voluptates.</p>
                          <?php
                       }
                          ?>             
                    </div>
                    <a href="<?php echo get_term_link(pll_get_term(28));?>" class="card-link text-uppercase" onchange="over()" onchange="out()">
                      <span class="card-link-text"><?php echo __( 'Beauty Services', 'road9' ) ; ?></span>
                      <span class="card-link-arrow text-uppercase">
                        <img src="<?php echo $button_icon; ?>" alt="arrow" class="px-2">
                      </span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /col-9 div -->
      </div>


    <!-- /row -->
    </div>
  <!-- /container div -->
  </div>
</section>

<!-- End of Provide-->

<!-- ####### Doctors ####### -->

<section id="doctors">
    <div class="container">
      <div class="row">
  
        
        <!-- ###### Side Text ####### -->
      
        <div class="col-lg-3 d-flex justify-content-center flex-column w-100">
            <p class="h3 text-uppercase font-weight-light"><?php echo $second_section_subtitle;?></p>
            <p class="h1 text-uppercase font-weight-bold pb-4"><?php echo $second_section_title;?></p>
            <p class="small para"><?php echo $second_section_description;?></p>
            <a href="<?php echo get_permalink(pll_get_post(208));?>" class="view-all-link py-3 text-uppercase"><?php echo __( 'View all doctors', 'road9' ) ; ?>
            <img src="wp-content/themes/road9/assets/img/Blue-arrow.png" alt="">
          </a>
        </div>
      
  
        <div class="col-lg-9" data-aos="fade-up" data-aos-duration="2000">
          <div class="row">
              <div class="owl-carousel owl-theme" id="owl-doctors">
              <?php
                $args = array(
                    'post_type' => 'doctor',
                    'orderby' => 'ID',
                    'order' => 'ASC',                                
                );
                
                $query = new WP_Query( $args );
                while ( $query->have_posts() ) :
                    $query->the_post();
                    $speciality = get_post_meta( get_the_ID(), 'mb_speciality' , true );
                    if($speciality){
                      $speciality = get_the_title( $speciality );
                    }
                    
                    ?>

                    <div class="card text-white">
                      <?php if(has_post_thumbnail()){ ?>
                        <img class="card-img" src="<?php the_post_thumbnail_url() ; ?>" alt="Card image">
                    <?php  }else{ ?>

                        <img class="card-img" src="http://via.placeholder.com/360x640" alt="Card image">
                    <?php } ?>
                      
                      
                      <div class="card-img-overlay">
                        <a href="<?php the_permalink(); ?>" class="card-link text-uppercase">
                          <span class="card-link-text"><?php echo __( 'More about doctors', 'road9' ) ; ?></span>
                        <span class="card-link-arrow text-uppercase"><img src="<?php echo $button_icon; ?>" alt="arrow" class="px-2"></span></a>
                          <div class="card-text-overlay">
                            <?php the_title('<h5 class="card-title font-weight-bold">', '</h5>'); ?>
                            <p class="card-text small"><?php echo $speciality; ?></p>
                          </div>

                      </div>
                    </div>
                    
                <?php
                endwhile; // End of the loop.
                wp_reset_postdata();
              ?>
          


            <!-- /row -->
            </div>
          </div>
        <!-- /col-10 div -->
        </div>
  
  
      <!-- /row -->
      </div>
    <!-- /container div -->
    </div>
    <div class="circle-doctors" data-aos="fade-up"  data-aos-duration="3000"></div>
</section>

<!-- ####### End of Doctors ####### -->
        
<!-- ######### Offers ######### -->

<section id="offers">
  <div class="container">
    <div class="row">

      
  <!-- ###### Side Text ####### -->
    
  <div class="col-lg-3 d-flex justify-content-center flex-column w-100">
      <p class="h3 text-uppercase font-weight-light"><?php echo $third_section_title ; ?></p>
      <p class="h1 text-uppercase font-weight-bold pb-4"><?php echo $third_section_subtitle ; ?></p>
      <p class="small para"><?php echo $third_section_description; ?></p>
      <a href="<?php echo get_permalink(pll_get_post(449));?>" class="view-all-link pt-2 text-uppercase"><?php echo __( 'VIEW ALL PROMOTIONS', 'road9' ) ; ?>             <img src="wp-content/themes/road9/assets/img/Blue-arrow.png" alt=""> 
      </a>
    </div>


  <div class="col-lg-9">
    <div class="row">
        <div class="owl-carousel owl-theme" id="owl-offers">
          <?php 
            $args = array(
                    'post_type' => 'promotions',
                    'orderby' => 'ID',
                    'order' => 'ASC',                                
              );
              $query = new WP_Query( $args );
              while ( $query->have_posts() ) :
                  $query->the_post();
                  $id = get_the_ID();
                  $promotions[] = $id;
          ?>
            <!--########## offer-1 ############-->
          <!-- Button trigger modal -->
          <div data-toggle="modal" data-target="#exampleModalCenter<?php echo get_the_ID(); ?>">
            <div class="card text-white">
              <img class="card-img" src="<?php the_post_thumbnail_url();?>" alt="Card image">
              <div class="card-img-overlay">
                  <div class="card-text-overlay">
                    <p class="card-text small"><?php echo wp_strip_all_tags( get_the_content() );?></p>
                  </div>

              </div>
          </div>
        </div>
          <?php
                endwhile; // End of the loop.
                wp_reset_postdata();
          ?>           


      <!-- /row -->
      </div>
    </div>
  <!-- /col-10 div -->
  </div>


<!-- /row -->
</div>
<!-- /container div -->
</div>
</section>

<!-- ######## End of Offers ####### -->

<?php foreach($promotions as $promotion){?>
    <div class="modal fade" id="exampleModalCenter<?php echo $promotion; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">    
                <div data-dismiss="modal" aria-label="Close">
                    <div class="close-circle"><span aria-hidden="true">&times;</span></div>
                </div>        
                <div class="modal-body">
                    <div class="card text-white">
                        <img class="card-img" src="<?php echo get_the_post_thumbnail_url($promotion);?>" alt="Card image">
                        <div class="card-img-overlay">
                            <div class="card-text-overlay">
                                <p class="card-text small"><?php echo wp_strip_all_tags( get_the_content() );?></p>
                            </div>            
                        </div>
                    </div>        
                </div>            
            </div>
        </div>
    </div>
<?php }?>

<!-- ######### News ######### -->

<section id="news">
    <div class="container">
      <div class="pb-5">
        <p class="h3 text-uppercase font-weight-light"><?php echo __( 'Our latest', 'road9' ) ; ?> </p>
        <p class="h1 text-uppercase font-weight-bold pb-4 news"><?php echo __( 'News', 'road9' ) ; ?></p>
        <a href="<?php echo get_term_link( pll_get_term(32 )); ?>" class="view-all-link text-uppercase"><?php echo __( 'VIEW ALL NEWS ', 'road9' ) ; ?>            
          <img src="wp-content/themes/road9/assets/img/Blue-arrow.png" alt="">
        </a>
      </div>
      
          <div class="row">
        <div class="col-md-12" data-aos="fade-up"
        data-aos-duration="2000">
          <div class="row">
              <div class="owl-carousel owl-theme" id="owl-news">
                <?php 
                  $args = array(
                          'post_type' => 'post',
                          'orderby' => 'ID',
                          'order' => 'ASC',
                          'category_name' => 'news',                                
                    );
                    $query = new WP_Query( $args );
                    while ( $query->have_posts() ) :
                        $query->the_post();
                        //$time = strtotime( get_field( 'date' ) );
                ?>
                 <div class="card text-white">
                  <img class="card-img" src="<?php the_post_thumbnail_url();?>" alt="Card image">
                  <div class="card-img-overlay">
                      <div class="card-date-news">
                          <h5 class="font-weight-bold mb-1"><?php echo get_the_date( 'd' );?></h5>
                          <h6><?php echo get_the_date( 'M' );?>.</h6>
                      </div>
                      <?php the_title('<p class="card-text p-3 font-weight-bold">','</p>');?>
                      
                        
                  </div>
              </div>
          <?php
                endwhile; // End of the loop.
                wp_reset_postdata();
          ?>   
            <!-- /row -->
            </div>
          </div>
        <!-- /col-10 div -->
        </div>
      
      
      <!-- /row -->
      </div>
      <!-- /container div -->
      </div>

</section>

<!-- ######### End of News ######## -->

<!-- ######## Map ######## -->
<section id="map" class="map-contact-us form">
        <iframe class="frame" src="https://snazzymaps.com/embed/161931"></iframe>
        <div class="container">
            
                      <?php
                        if(get_locale()== 'en_GB'){
		                      echo do_shortcode('[contact-form-7 id="265" title="Contact us"]');
		                    }else{
						            	echo do_shortcode('[contact-form-7 id="266" title="Contact us Arabic"]');
		                    }
                      ?>              
                  
        </div>
      
      </section>
      <!-- ##### End of Map ####### -->



<?php



get_footer();
?>