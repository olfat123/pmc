<?php
/**
 * default search form
 */
?>
<form class="form-inline md-form form-sm active-cyan-0 pb-3" method="get" action="<?php echo home_url(); ?>">
    <input class="form-control form-control-sm search" id="search" type="text" value="<?php echo get_search_query() ?>" name="s" placeholder="Search" aria-label="Search">
    <div class="input-group-append">
        <button type="submit" id="search-btn"><span class="input-group-text"><img src="<?php echo site_url(); ?>/wp-content/uploads/2019/06/search.png" alt=""></span></button>
    </div>
</form>