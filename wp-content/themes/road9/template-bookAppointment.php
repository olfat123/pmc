<?php /* Template Name: Book Appointment */
get_header();
?>
<div class="circle-sub" style="z-index: -1"></div>

<div class="container-fluid p-5">
    <?php echo the_title(' <p class="text-uppercase page-sub-title">', '</p>'); ?>  
    <nav class="breadCrumbNav">
        <?php 
        breadcrumbs() ;
        ?>
    </nav>
</div>
<section id="book-appointment" class="pt-5 form">
  <div class="row pt-5">
    
      <?php
        while(have_posts()):
          the_post();
          ?>
          <div class="col-md-4">
            <img src="<?php the_post_thumbnail_url(); ?>" alt="">
          </div>
          <div class="col-md-pull-4 col-md-8">
            <div class="container">
                <h5 class="text-uppercase h5 font-weight-bold pb-5"><?php echo __( 'booking form', 'road9' ) ; ?></h5>
                <?php the_content(); ?>            
            </div>
          </div>
          <?php
         
        endwhile;
      ?>

        
    
  </div>
</section>
<?php
get_footer();
?>