<?php /* Template Name: FAQ */ 
get_header();

?>
<div class="circle-sub" style="z-index: -1"></div>
<div class="container-fluid p-5">
    <?php echo the_title(' <p class="text-uppercase page-sub-title">', '</p>'); ?>  
    <nav class="breadCrumbNav">
        <?php 
        breadcrumbs() ;
        ?>
    </nav>
</div>

<section id="faqs">
  <div class="container">

    <?php
       $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        $args = array(
            'post_type' => 'faqs',
            'posts_per_page' => 10,
            'paged' => $paged,
            'orderby' => 'ID',
            'order' => 'ASC',
        );
        query_posts($args); 
        ?>
        
        <?php
        while ( have_posts() ) : the_post();
          ?>  

          <!-- #### data-target, aria-controls, span[data-target] all should have a unique id ####-->

            <div class="accordion" id="accordionExample">
              <div class="card mb-4">
                <div class="card-header" id="headingOne">
                <?php the_title('<p class="font-weight-bold">', '</p>'); ?>                            
                    <button id="collapse-btn" class="btn btn-link collapse-btn" type="button" data-toggle="collapse" data-target="#collapse<?php echo get_the_ID(); ?>" aria-expanded="false" aria-controls="collapse<?php echo get_the_ID(); ?>">
                      <span aria-expanded="false" data-toggle="collapse" data-target="#collapse<?php echo get_the_ID(); ?>" class="collapse-arrow"></span>
                    </button>
                </div>
            
                <!-- #### id increments id="collapse{unique id}" #### -->
                
                <div id="collapse<?php echo get_the_ID(); ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <div class="card-body small para"><?php the_content('',''); ?> </div>

                <!-- #### the_content classes not taking effect #### -->
                </div>
              </div>
            </div>

          <?php

        endwhile; // End of the loop.
        wp_reset_postdata();
        core_paging_nav();
        ?>
  </div>
</section>
            
            
<?php
 
get_footer();