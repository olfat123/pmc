<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Road9
 * @since 1.0.0
 */

get_header();
?>
<div class="circle-sub" style="z-index: -1"></div>
<div class="container-fluid p-5">
    <?php 
    $cat = get_the_category();
    //print_r($cat);
     //$cat = $cat[0]; ?>  
    <p class="text-uppercase page-sub-title"><?php echo $cat[0]->cat_name; ?></p>
    <nav class="breadCrumbNav">
        <?php 
        breadcrumbs() ;
        ?>
    </nav>
</div>
<?php
/* Start the Loop */
while ( have_posts() ) :
    the_post();?>
  

   <section id="news-single">
      <div class="container py-5">
        <div class="d-flex flex-column justify-content-center">
          <img src="<?php the_post_thumbnail_url(); ?>" alt="" class="img-fluid align-self-center" width="740px" height="auto"> 
          <h6 class="h6 w-50 text-center font-weight-bold pt-4 align-self-center"><?php the_title();?></h6>
          <p class="small para text-capitalize align-self-center pt-4 light-grey"><?php the_date(); ?></p>

           <?php the_content(); ?>
        </div>

        <div class="social-icons pt-5">
                <span class="text-uppercase text-grey">share</span>
                <a href="http://www.facebook.com/sharer.php?u=<?php esc_url(the_permalink()); ?>" class="px-4" target="_blank"><i class="fab fa-facebook-f"></i></a>
                <a href="https://twitter.com/share?url=<?php esc_url(get_permalink()); ?>"class="px-4" target="_blank"><i class="fab fa-twitter"></i></a>
                <a href="http://www.linkedin.com/shareArticle?url=<?php esc_url(the_permalink()); ?>"class="px-4" target="_blank"><i class="fab fa-linkedin-in"></i></a>
               
        </div>

        <section id="news-page">
          <div class="container">
            <div class="row">       
              <h5 class="mx-auto font-weight-bold h3 text-uppercase p-5"><?php echo __( 'More', 'road9' ) ; ?> <?php echo $cat[0]->cat_name; ?>s</h5>
              <div class="col-md-12" data-aos="fade-up" data-aos-duration="2000">
                <div class="row">
                  <?php     
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' =>'2',
                        'category_name'=> $cat[0]->cat_name
                    );
                    $query = new WP_Query( $args );
                    while ( $query->have_posts() ) : $query->the_post(); ?>                 
                      <div class="col-md-6 pb-5">
                        <div class="card bg-dark text-white">
                          <img class="card-img" src="<?php the_post_thumbnail_url(); ?>" alt="Card image">
                          <div class="card-img-overlay">
                              <div class="card-date-news">
                                  <h5 class="font-weight-bold mb-1"><?php echo get_the_date( 'd' );?></h5>
                                  <h6><?php echo get_the_date( 'M' );?>.</h6>
                              </div>
                              <a href="<?php the_permalink(); ?>"
                                <p class="card-text p-3 font-weight-bold"><?php the_title(); ?></p>
                              </a>
                          </div>
                        </div>
                      </div>  
                    <?php 
                    endwhile; // End of the loop.
                    wp_reset_postdata();
                    ?>              
                </div>            
              </div>          
            </div>              
          </div>           
        </section>

      </div>
    </section>
<?php

endwhile; // End of the loop.


get_footer();