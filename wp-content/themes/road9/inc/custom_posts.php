<?php
/////////////// Register custom posts /////////////////////
function create_posttype() {
    


    register_post_type( 'insurance_companies',
    // CPT Options
        array(
            'supports' => array(   
                'title', // post title             
                'author', // post author
                'thumbnail', // featured images                
                'revisions', // post revisions
                'post-formats', // post formats
            ),
            'labels' => array(
                'name' => __( 'Insurance Companies' , 'road9'),
                'singular_name' => __( 'Company' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'insurance-companies'),
            
        )
    );

    register_post_type( 'speciality',
    // CPT Options
        array(
            'supports' => array(
                'title', // post title
                'editor', // post content
                'author', // post author
                'thumbnail', // featured images
                'excerpt', // post excerpt
                'custom-fields', // custom fields
                'comments', // post comments
                'revisions', // post revisions
                'post-formats', // post formats
            ),
            'labels' => array(
                'name' => __( 'Specialities' ,'road9'),
                'singular_name' => __( 'Speciality' ,'road9')
            ),            
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'speciality'),
            
        )
    );

    register_post_type( 'doctor',
    // CPT Options
        array(
            'supports' => array(
                'title', // post title
                'editor', // post content
                'author', // post author
                'thumbnail', // featured images
                'excerpt', // post excerpt
                'custom-fields', // custom fields
                'comments', // post comments
                'revisions', // post revisions
                'post-formats', // post formats
            ),
            'labels' => array(
                'name' => __( 'Doctors' ,'road9'),
                'singular_name' => __( 'Doctor' ,'road9')
            ),            
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'doctor'),
            
        )
    );

    register_post_type( 'medical_services',
    // CPT Options
        array(
            'supports' => array(
                'title', // post title
                'editor', // post content
                'author', // post author  
                'thumbnail', // featured images                     
                'excerpt', // post excerpt
                'custom-fields', // custom fields
                'comments', // post comments
                'revisions', // post revisions
                'post-formats', // post formats                
            ),
            'labels' => array(
                'name' => __( 'Medical Services' ,'road9'),
                'singular_name' => __( 'Medical Service' ,'road9')
            ),
            'taxonomies' => array( 'medical_service_category' ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'medical_services'),
            
        )      
    );

    register_post_type( 'beauty_services',
    // CPT Options
        array(
            'supports' => array(
                'title', // post title
                'editor', // post content
                'author', // post author  
                'thumbnail', // featured images                     
                'excerpt', // post excerpt
                'custom-fields', // custom fields
                'comments', // post comments
                'revisions', // post revisions
                'post-formats', // post formats
            ),
            'labels' => array(
                'name' => __( 'Beauty Services' ,'road9'),
                'singular_name' => __( 'Beauty Service' ,'road9')
            ),
            'taxonomies' => array( 'beauty_service_category' ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'beauty_service'),
            
        )
    );

    register_post_type( 'promotions',
    // CPT Options
        array(
            'supports' => array(
                'title', // post title
                'editor', // post content
                'author', // post author
                'thumbnail', // featured images                
                'revisions', // post revisions
                'post-formats', // post formats
            ),
            'labels' => array(
                'name' => __( 'Promotions' ,'road9'),
                'singular_name' => __( 'promotion' ,'road9')
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'promotion'),
            
        )
    );
    register_post_type( 'faqs',
    // CPT Options
        array(
            'supports' => array(
                'title', // post title
                'editor', // post content
                'author', // post author                
                'excerpt', // post excerpt
                'custom-fields', // custom fields
                'comments', // post comments
                'revisions', // post revisions
                'post-formats', // post formats
            ),
            'labels' => array(
                'name' => __( 'Frequency Ask Questions' ,'road9' ),
                'singular_name' => __( 'Question' ,'road9')
            ),
            
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'frequency-ask-questions'),
            
        )
    );
    
}    
add_action( 'init', 'create_posttype' );

/////////////register question taxonomy for solution posts //////////////
add_action( 'init', 'create_medical_service_custom_taxonomy', 0 );

function create_medical_service_custom_taxonomy() {
 
  $labels = array(
    'name' => _x( 'medical service categories', 'taxonomy general name' ),
    'singular_name' => _x( 'category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search categories' ),
    'all_items' => __( 'All categories' ),
    'parent_item' => __( 'Parent category' ),
    'parent_item_colon' => __( 'Parent category:' ),
    'edit_item' => __( 'Edit category' ), 
    'update_item' => __( 'Update category' ),
    'add_new_item' => __( 'Add New category' ),
    'new_item_name' => __( 'New category Name' ),
    'menu_name' => __( 'Medical Service Categories' ),
  ); 	
 
  register_taxonomy('medical_service_category',array('medical_services'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'medical-service-category'),
  ));
}

/////////////register beauty taxonomy for solution posts //////////////
add_action( 'init', 'create_beauty_custom_taxonomy', 0 );

function create_beauty_custom_taxonomy() {
 
  $labels = array(
    'name' => _x( 'Beauty Service Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search categorys' ),
    'all_items' => __( 'All categorys' ),
    'parent_item' => __( 'Parent category' ),
    'parent_item_colon' => __( 'Parent category:' ),
    'edit_item' => __( 'Edit category' ), 
    'update_item' => __( 'Update category' ),
    'add_new_item' => __( 'Add New category' ),
    'new_item_name' => __( 'New category Name' ),
    'menu_name' => __( 'Beauty Service Categories' ),
  ); 	
 
  register_taxonomy('beauty_service_category',array('beauty_services'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'beauty-service-category' ),
  ));
}

//////////// Create date metabox for event post type /////////////
function ep_eventposts_metaboxes() {
    add_meta_box( 'ept_event_date', 'News Date', 'ept_event_date', 'news', 'side', 'default', array( 'id' => '_start') );
    //add_meta_box( 'ept_event_date_end', 'End Date and Time', 'ept_event_date', 'event', 'side', 'default', array('id'=>'_end') );
   // add_meta_box( 'ept_event_location', 'Event Location', 'ept_event_location', 'event', 'side', 'default', array('id'=>'_end') );
}
add_action( 'admin_init', 'ep_eventposts_metaboxes' );

function ept_event_date($post, $args) {
    $metabox_id = $args['args']['id'];
    global $post, $wp_locale;  
  
    $time_adj = current_time( 'timestamp' );
    $month = get_post_meta( $post->ID, $metabox_id . '_month', true );
  
    if ( empty( $month ) ) {
        $month = gmdate( 'm', $time_adj );
    }
  
    $day = get_post_meta( $post->ID, $metabox_id . '_day', true );  
    if ( empty( $day ) ) {
        $day = gmdate( 'd', $time_adj );
    }
  
    $year = get_post_meta( $post->ID, $metabox_id . '_year', true );  
    if ( empty( $year ) ) {
        $year = gmdate( 'Y', $time_adj );
    }
  
    $hour = get_post_meta($post->ID, $metabox_id . '_hour', true);  
    if ( empty($hour) ) {
        $hour = gmdate( 'H', $time_adj );
    }
  
    $min = get_post_meta($post->ID, $metabox_id . '_minute', true);  
    if ( empty($min) ) {
        $min = '00';
    }
  
    $month_s = '<select name="' . $metabox_id . '_month">';
    for ( $i = 1; $i < 13; $i = $i +1 ) {
        $month_s .= "\t\t\t" . '<option value="' . zeroise( $i, 2 ) . '"';
        if ( $i == $month )
            $month_s .= ' selected="selected"';
        $month_s .= '>' . $wp_locale->get_month_abbrev( $wp_locale->get_month( $i ) ) . "</option>\n";
    }
    $month_s .= '</select>';
  
    echo $month_s;
    echo '<input type="text" name="' . $metabox_id . '_day" value="' . $day  . '" size="2" maxlength="2" />';
    echo '<input type="text" name="' . $metabox_id . '_year" value="' . $year . '" size="4" maxlength="4" />';
    echo '<input type="text" name="' . $metabox_id . '_hour" value="' . $hour . '" size="2" maxlength="2"/>:';
    echo '<input type="text" name="' . $metabox_id . '_minute" value="' . $min . '" size="2" maxlength="2" />';
  
}
function save_event_meta( $post_id ) {
    
    $day_old = get_post_meta( $post_id, '_start_day' , true );
    $month_old = get_post_meta( $post_id, '_start_month' , true );
    $year_old = get_post_meta( $post_id, '_start_year' , true );
    $hour_old = get_post_meta( $post_id, '_start_hour' , true );
    $minute_old = get_post_meta( $post_id, '_start_minute' , true );
    $day_new = $_POST['_start_day'];
    $month_new = $_POST['_start_month'];
    $year_new = $_POST['_start_year'];
    $hour_new = $_POST['_start_hour'];
    $minute_new = $_POST['_start_minute'];

    if ( $day_new && $day_new !== $day_old ) {
        update_post_meta( $post_id, '_start_day', $day_new );
    } elseif ( '' === $day_new && $day_old ) {
        delete_post_meta( $post_id, '_start_day', $day_old );
    }
    if ( $month_new && $month_new !== $month_old ) {
        update_post_meta( $post_id, '_start_month', $month_new );
    } elseif ( '' === $month_new && $month_old ) {
        delete_post_meta( $post_id, '_start_month', $month_old );
    }
    if ( $year_new && $year_new !== $year_old ) {
        update_post_meta( $post_id, '_start_year', $year_new );
    } elseif ( '' === $year_new && $year_old ) {
        delete_post_meta( $post_id, '_start_year', $year_old );
    }    
    if ( $hour_new && $hour_new !== $hour_old ) {
        update_post_meta( $post_id, '_start_hour', $hour_new );
    } elseif ( '' === $hour_new && $hour_old ) {
        delete_post_meta( $post_id, '_start_hour', $hour_old );
    }
    if ( $minute_new && $minute_new !== $minute_old ) {
        update_post_meta( $post_id, '_start_minute', $minute_new );
    } elseif ( '' === $minute_new && $minute_old ) {
        delete_post_meta( $post_id, '_start_minute', $minute_old );
    }
}
add_action( 'save_post', 'save_event_meta' );

///////// Create metaboxes for testimonial post type ////////////
function mb_doctorposts_metaboxes() {
    add_meta_box( 'mb_speciality', 'Speciality', 'mb_speciality', 'doctor', 'side', 'default', array( 'id' => 'mb_speciality') );
    }
add_action( 'admin_init', 'mb_doctorposts_metaboxes' );

function mb_speciality($post, $args) {
    //wp_nonce_field( 'road9_testimonial_metabox_nonce', 'road9_testimonial_nonce' );
    $metabox_id = $args['args']['id'];
    global $post, $wp_locale;
    $speciality = get_post_meta( $post->ID, $metabox_id , true ); 
    
    $specialities = '<select name="' . $metabox_id . '"   >
    <option value="">Select Speciality </option> '; 
    $args = array(
        'post_type' => 'speciality',                          
    );
    $query = new WP_Query( $args );
                   
    while ( $query->have_posts() ) :
        // Start the Loop.			
    $query->the_post();
    if( $speciality == get_the_ID()){
        $selected = 'selected';
        
    }else{
        $selected =""; 
    }
    $id = get_the_ID();
    $title = get_the_title($id);
    $specialities .= '<option value="' . get_the_ID()  . '"' .$selected.'>'.$title.'</option>';
    
    endwhile;
    wp_reset_postdata();
    
    $specialities .= '</select>';
    echo $specialities;
}


function save_doctor_meta( $post_id ) {
   
    $speciality_old = get_post_meta( $post_id, 'mb_speciality' , true );
    $speciality_new = $_POST['mb_speciality'];    

    if ( $speciality_new && $speciality_new !== $speciality_old ) {
        update_post_meta( $post_id, 'mb_speciality', $speciality_new );
    } elseif ( '' === $speciality_new && $speciality_old ) {
        delete_post_meta( $post_id, 'mb_speciality', $speciality_old );
    }
   
}
add_action( 'save_post', 'save_doctor_meta' );

///////// Create metaboxes for specialityimage post type ////////////
function mb_specialityimageposts_metaboxes() {
    add_meta_box( 'mb_specialityimage', 'Cover Image', 'mb_specialityimage', 'speciality', 'side', 'default', array( 'id' => 'mb_specialityimage') );
    }
add_action( 'admin_init', 'mb_specialityimageposts_metaboxes' );

function mb_specialityimage($post, $args){
    $metabox_id = $args['args']['id'];
    global $post, $wp_locale;
    $image = get_post_meta( $post->ID, $metabox_id , true );
    echo '<p>
            <label for="image">Image Upload</label><br>
            <input class="meta-image" type="text" name="' . $metabox_id . '"   value="'. $image.'">
            <input type="button" class="button image-upload" value="Browse">
        </p>
        <div class="image-preview"><img src="'. $image.'" style="max-width: 250px;"></div>
        ';
}

function save_specialityimage_meta( $post_id ) {
   
    $logo_old = get_post_meta( $post_id, 'mb_specialityimage' , true );
    if (isset($_POST['mb_specialityimage'])){
        $logo_new = $_POST['mb_specialityimage'];      
    }
    
    if($logo_new){
        if ( $logo_new !== $logo_old ) {
            update_post_meta( $post_id, 'mb_specialityimage', $logo_new );
        } elseif ( '' === $logo_new && $logo_old ) {
            delete_post_meta( $post_id, 'mb_specialityimage', $logo_old );
        }
    }    
   
}
add_action( 'save_post', 'save_specialityimage_meta' );
?>