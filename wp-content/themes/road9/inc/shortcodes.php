<?php 
 
function sponsers(){

    $print = '<section id="insurance">
    <div class="container">
      <div class="row"> 
      <div class="owl-carousel owl-theme" id="owl-insurance">';
    
    
        $args = array(
            'post_type' => 'insurance_companies',
            'orderby' => 'ID',
            'order' => 'ASC',                                
        );
        $query = new WP_Query( $args );
        while ( $query->have_posts() ) :
            $query->the_post();
                
            $print .= '<div><img src="'.get_the_post_thumbnail_url().'" alt=""></div>';


         endwhile;
         $print .='</div></div></div></section>';

        echo $print;
  
}

add_shortcode('print_sponsers', 'sponsers');

function bookAppointment($post){
  $id = $post->ID;
  $type = get_post_type($id);
  if($type == 'doctor'){
    $id = get_post_meta( get_the_ID(), 'mb_speciality' , true );    
  }
  global $road9;
  $phone = $road9['phone-text'];
  $print ='<div class="booking-div">
  <div class="">    
    <div class="d-flex justify-content-end">
        <div class="book"><p><a href="'.get_permalink(pll_get_post(382)).'?selected-speciality='.get_the_title($id).'">'.__( "Book an appointment", "road9" ).'</a></p></div>
     <div class="phone"><p class="text-capitalize font-weight-bold pt-1">'.__('Phone','road9').'</p><p>'.$phone.'</p></div>     
    </div>
  </div>
</div>';

echo $print;
}

add_shortcode('bookAppointment', 'bookAppointment');

function add_map(){
  $print = '
    
<section id="map" class="map-contact-us form">
        <iframe class="frame" src="https://snazzymaps.com/embed/161931"></iframe>
        <div class="container">
            <div class="send-message" data-aos="fade-up"
                  data-aos-duration="2000">
      
                  <div class="container p-5">
                      <div class="h5 text-uppercase font-weight-bold pb-3"><?php echo __( "Send Us A Message", "road9" ) ; ?></div>
                      '.get_the_content().'
                  </div> 
      
            </div>
        </div>
      
      </section>
      
  ';

  echo $print;
}

add_shortcode('add_map', 'add_map');
