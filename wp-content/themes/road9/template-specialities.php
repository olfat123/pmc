<?php /* Template Name: Specialities */ 
get_header();
global $road9;
$image = $road9['specialities-image']['url'];
?>
<div class="circle-sub" style="z-index: -1"></div>

<div class="container-fluid p-5">
    <?php echo the_title(' <p class="text-uppercase page-sub-title">', '</p>'); ?>  
    <nav class="breadCrumbNav">
        <?php 
        breadcrumbs() ;
        ?>
    </nav>
</div>
    
    <?php
    
    $args = array(
        'post_type' => 'speciality',
        'posts_per_page' => -1,
        'orderby' => 'ID',
        'order' => 'ASC',
        
    );
    $query = new WP_Query( $args );
    $specialities = array();
    while ( $query->have_posts() ) :
        $query->the_post();                       
        $specialities[] = get_the_ID();
                        

    endwhile; // End of the loop.
    wp_reset_postdata();
?>        
<section id="specialties">
    <div class="row">
        <div class="col-md-4">
            <ul class="specialties-nav">
                
                <?php 
                foreach($specialities as $speciality){ ?>
                    <li class="text-capitalize <?php if($speciality == $specialities[0]){ echo 'active'; } ?> tab-link">
                        <img src="<?php echo get_the_post_thumbnail_url($speciality);?>" alt="icon" class="px-2">
                        <a href="<?php echo get_permalink($speciality);?>"><?php echo get_the_title($speciality); ?></a>
                    </li>
                <?php
                }?>
            </ul>

            <select name="side-menu" id="side-menu" class="specialties-select">
                <option class="text-capitalize text-muted px-3" selected disabled>Specialties Clinics</option>
            <?php 
                foreach($specialities as $speciality){ ?>
                <option class="main-blue text-capitalize px-3" value="<?php echo get_permalink($speciality);?>">
                    <img src="<?php echo get_the_post_thumbnail_url($speciality);?>" alt="icon" class="px-2">
                    <?php echo get_the_title($speciality);?>
                </option>
            <?php
            }?>
            </select>

        </div>
                     
                
        <div class="col-md-8 d-block">
            <div class="content">
                <img src="<?php echo $image; ?>" alt="tab-img" class="tab-img"><div class="overlay"></div>
                <div class="container">
                    <h5 class="h5"><img src="<?php echo get_the_post_thumbnail_url($specialities[0]);?>" alt="" class="px-2 pb-2"><?php echo get_the_title($specialities[0]);?> clinic</h5>
                    <p class="small para"><?php echo get_post_field('post_content',$specialities[0]);?></p>
                    


                    <h6 class="h6 pt-5"> <?php echo get_the_title($specialities[0]);?> Doctors</h6>
                    <section id="doctors">
                        <div class="container">
                            <div class="row">
                                <div class="owl-carousel owl-theme" id="owl-doctors-specialties">
                                <?php
                                $args = array(
                                    'post_type' => 'doctor',
                                    'posts_per_page' => -1,
                                    'meta_query' => array(
                                        'relation' => 'OR',
                                    array(
                                        'key'      => 'mb_speciality',
                                        'value'    => $specialities[0],
                                        'compare'       => 'LIKE'
                                    )  )                             
                                );
                                $query = new WP_Query( $args );
                                while ( $query->have_posts() ) :
                                    $query->the_post();
                                    ?>                                                
                                        <div class="card text-white">
                                        <?php if(has_post_thumbnail()){ ?>
                                            <img class="card-img" src="<?php the_post_thumbnail_url() ; ?>" alt="Card image">
                                            <?php  }else{ ?>
                                                <img class="card-img" src="http://via.placeholder.com/360x640" alt="Card image">
                                            <?php } ?>
                                            <div class="card-img-overlay">
                                                <a href="<?php the_permalink(); ?>" class="card-link text-uppercase"><span class="card-link-text">More about doctors</span><span class="card-link-arrow text-uppercase"><img src="<?php echo home_url(); ?>/wp-content/themes/road9/assets/img/arrow.png" alt="arrow" class="px-2"></span></a>
                                                <div class="card-text-overlay">
                                                    <h5 class="card-title font-weight-bold"><?php the_title(); ?></h5>
                                                    <p class="card-text small"><?php echo get_the_title($specialities[0]);?></p>
                                                </div>

                                            </div>
                                        </div>  
                                <?php endwhile;
                                wp_reset_postdata();?>       
                            </div>
                        </div>  
                    </section>
                
                </div>   
            </div>


            
        </div>
    </div>
    <div class="col-md-12 d-block">
<!-- ######### Book and appointment div -->
                <?php do_shortcode('[bookAppointment]'); ?>
              <!-- ######### Book and appointment div -->
        </div>
</section>
  
<?php
get_footer();
?>

