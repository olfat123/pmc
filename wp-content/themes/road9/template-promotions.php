<?php /* Template Name: Promotions */ 
get_header();
?>
<div class="circle-sub" style="z-index: -1"></div>

<div class="container-fluid p-5">
    <?php echo the_title(' <p class="text-uppercase page-sub-title">', '</p>'); ?>  
    <nav class="breadCrumbNav">
        <?php 
        breadcrumbs() ;
        ?>
    </nav>
</div>
<?php do_shortcode('[bookAppointment]'); ?>

<section id="offers" class="promotions-page">
    <div class="container">
        <div class="row">
            <?php
                $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                $args = array(
                    'post_type' => 'promotions',
                    'posts_per_page' => 2,
                    'paged' => $paged, 
                    'orderby' => 'ID',
                    'order' => 'ASC',        
                );
                query_posts($args); 
                $promotions = array();              
                while ( have_posts() ) :                    
                    the_post();
                    $id = get_the_ID();
                    $promotions[] = $id;
                    ?>
                    <div class="col-lg-3 col-md-6 mb-4">
                        <div data-toggle="modal" data-target="#exampleModalCenterOne<?php echo $id; ?>">
                            <div class="card text-white">
                            <img class="card-img" src="<?php the_post_thumbnail_url();?>" alt="Card image">
                            <div class="card-img-overlay">
                                <div class="card-text-overlay">
                                    <p class="card-text small"><?php echo wp_strip_all_tags( get_the_content() );?></p>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <?php
                endwhile; // End of the loop.
            ?>


<div class="container">
<?php                 
wp_reset_postdata();
core_paging_nav(); 
?>
</div>

        </div>
    </div>
</section>
<?php foreach($promotions as $promotion){?>
    <div class="modal fade" id="exampleModalCenterOne<?php echo $promotion; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">    
                <div data-dismiss="modal" aria-label="Close">
                    <div class="close-circle"><span aria-hidden="true">&times;</span></div>
                </div>        
                <div class="modal-body">
                    <div class="card text-white">
                        <img class="card-img" src="<?php echo get_the_post_thumbnail_url($promotion);?>" alt="Card image">
                        <div class="card-img-overlay">
                            <div class="card-text-overlay">
                                <p class="card-text small"><?php echo wp_strip_all_tags( get_the_content() );?></p>
                            </div>            
                        </div>
                    </div>        
                </div>            
            </div>
        </div>
    </div>
<?php }?>

    
 
<?php


get_footer();
?>