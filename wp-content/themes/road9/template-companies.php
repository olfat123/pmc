<?php /* Template Name: Insurance Companies */ 
get_header();


?>
<div class="circle-sub" style="z-index: -1"></div>
<div class="container-fluid p-5">
    <?php echo the_title(' <p class="text-uppercase page-sub-title">', '</p>'); ?>  
    <nav class="breadCrumbNav">
        <?php 
        breadcrumbs() ;
        ?>
    </nav>
</div>

<section id="insurance-logos" class="pb-5">
  <div class="container pb-5">
    <div class="row">
        <?php 
    
        $args = array(
            'post_type' => 'insurance_companies',
            'posts_per_page' =>'-1'
        );
        $query = new WP_Query( $args );
                
        while ( $query->have_posts() ) : $query->the_post(); 
        ?>  
        <div class="col-lg-2 col-md-4 col-6">
            <img src="<?php the_post_thumbnail_url(); ?>" alt="">
            <p class="logo-text">next care</p>
        </div>                                     
        <?php 
        endwhile; // End of the loop.
        wp_reset_postdata();
        ?>
    </div>
  </div>
</section>
            
            
           
<?php
get_footer();
?>