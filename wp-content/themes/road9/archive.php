<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Road9
 * @since 1.0.0
 */

get_header();
$obj = get_queried_object();
$post_type = get_post_type();
$posts_ids = array();

if ( $post_type )
{
	if($post_type == 'medical_services' || $post_type == 'beauty_services'){
		
			// Start the Loop.
			while ( have_posts() ) : the_post();
				$posts_ids[] = get_the_ID();
				
			endwhile;
			wp_reset_postdata();

		?>
		<div class="container-fluid p-5">
		    <?php echo '<p class="text-uppercase page-sub-title">'.$obj -> name. '</p>'; ?>
		    <nav class="breadCrumbNav">
		        <?php 
		        breadcrumbs() ;
		        ?>
		    </nav>
		</div>
		<section id="tabs-page">
			<div class="row">
				<div class="col-md-4">
					<!-- Tabs -->
					<div class="nav flex-column nav-pills nav-tabs" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<?php
						// Start the Loop.
						foreach($posts_ids as $post_id)	{
							?>
							<a class="nav-link" id="v-pills-<?php echo $post_id;?>-tab" data-toggle="pill" href="#tab-<?php echo $post_id;?>" data-tab="tab-<?php echo $post_id;?>" role="tab" aria-controls="v-pills-<?php echo $post_id;?>" aria-selected="true"><?php echo get_the_title($post_id);?>
								
							</a>
							<?php
						}
						?>
					</div>


					<!-- Dropdown Responsive tabs -->
					<select name="side-menu" id="side-menu" class="specialties-select">
	                	<option class="text-capitalize text-muted px-3" selected disabled><?php echo __( 'Our Services', 'road9' ) ; ?></option>
						<?php 
							foreach($posts_ids as $post_id)	{ ?>
							<option class="main-blue text-capitalize px-3" value="#tab-<?php echo $post_id;?>">
								<?php echo get_the_title($post_id);?>
							</option>
						<?php
						}?>
	            	</select>

				</div>
			
				<div class="col-md-8">
					<div class="container">
						<div class="tab-content" id="v-pills-tabContent">
						<?php
							// Start the Loop.
							foreach($posts_ids as $post_id)	{	
							$content = get_post_field('post_content', $post_id);			
								?>						
								<div class="tab-pane" id="tab-<?php echo $post_id; ?>" role="tabpanel" aria-labelledby="v-pills-<?php echo $post_id; ?>-tab">			
									<img src="<?php echo get_the_post_thumbnail_url($post_id);?>" alt="" style="z-index: -2">		
									<div class="overlay"></div>
									<h5 class="h5 pt-5"><?php echo get_the_title($post_id); ?></h5>
									<p class="small para font-weight-bold">	<?php echo $content ; ?></p>
									
								</div>
								<?php					
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php
				
	}
	else{
		?>
		<div class="circle-sub" style="z-index: -1"></div>
		<div class="grey-section-full-page"></div>
		<!-- after-header -->
		<div class="container-fluid p-5">
		   <?php echo '<p class="text-uppercase page-sub-title">'.$obj -> name. '</p>'; ?>  
		    <nav class="breadCrumbNav">
		        <?php 
		        breadcrumbs() ;
		        ?>
		    </nav>
		</div>
		<section id="news-page">
		    <div class="grey-section-full-page"></div>
	        <div class="container">
	            <div class="row">
		            <div class="col-md-12" data-aos="fade-up" data-aos-duration="1000">
		              	<div class="row">
		              		<?php
		              			// Start the Loop.
									while ( have_posts() ) : the_post(); ?>	
										<div class="col-md-6 pb-5">
										<a href="<?php the_permalink(); ?>">
					                      	<div class="card text-white">
											  <a href="<?php the_permalink(); ?>"><img class="card-img" src="<?php the_post_thumbnail_url(); ?>" alt="Card image"></a>
					                        	<div class="card-img-overlay">
					                            	<div class="card-date-news">
					                                	<h5 class="font-weight-bold mb-1"><?php echo get_the_date( 'd' );?></h5>
					                                	<h6><?php echo get_the_date( 'M' );?>.</h6>
					                            	</div>
					                            	<a href="<?php the_permalink(); ?>">
					                            	<?php the_title('<p class="card-text p-3 font-weight-bold">','</p>'); ?>     	
					                              	</a>
												</div>
											  </div>
											  </a>
						                </div>
  
										<?php
									endwhile;
									wp_reset_postdata();
		              		?>
		              	</div>
		            </div>
		        </div>
		    </div>
		</section>
		<div class="container">
			<?php                 

			core_paging_nav(); 
			?>
		</div>
		<?php
	}
}



        
		
	

get_footer();
