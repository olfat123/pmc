<?php
require get_template_directory() . '/admin-folder/admin/admin-init.php';
// Register Custom Navigation Walker for function.php
require_once('wp_bootstrap_navwalker.php');
require get_template_directory() . '/inc/custom_posts.php';
require get_template_directory() . '/inc/shortcodes.php';

// Enable support for post thumbnails on posts and pages
add_theme_support( 'post-thumbnails' );
add_filter( 'wpcf7_support_html5_fallback', '__return_true' );

// Register theme menues
register_nav_menus(
    array(
        'menu-1' => __( 'Primary', 'road9' ),
        'top-menu' => __( 'Top Menu', 'road9' ),
        'social' => __( 'Social Links Menu', 'road9' ),
    )
);

/*
* Switch default core markup for search form, comment form, and comments
* to output valid HTML5.
*/
add_theme_support(
    'html5',
    array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    )
);

/**
 * Add support for core custom logo.
 *
 */
add_theme_support(
    'custom-logo',
    array(
        'height'      => 190,
        'width'       => 190,
        'flex-width'  => false,
        'flex-height' => false,
    )
);
//add_theme_support( 'title-tag' );

/******** add element to menu ***********/
add_filter( 'wp_nav_menu_items', 'your_custom_menu_item', 10, 2 );
function your_custom_menu_item ( $items, $args ) {
    if ($args->theme_location == 'menu-1') {
        $items .= '<li class="nav-item active w-50 d-none"><a class="nav-link menu-icon" href="#" onclick="openMenu()"><i class="fas fa-bars"></i> Menu<span class="sr-only">(current)</span></a>
          </li>';
    }
    return $items;
}

/**
 * Enqueue scripts and styles.
 */
function road9_scripts() { 

	  wp_enqueue_style( 'road9-style', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) ); 
    wp_enqueue_style( 'bootstrap-css', "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css", array(), '4.3.1' );
   
    if ( is_rtl() ) {
      wp_enqueue_style(  'style-rtl',   get_template_directory_uri() . '/assets/css/styles_rtl.css', array(), '1.0.0') ;
    }else{
       wp_enqueue_style( 'styles-css', get_template_directory_uri() . '/assets/css/styles.css', array(), '1.0.0' );
    }
    wp_enqueue_style( 'fontawesome-css', "https://use.fontawesome.com/releases/v5.8.2/css/all.css", array(), '5.8.2' );
    wp_enqueue_style( 'aos-css', "https://unpkg.com/aos@next/dist/aos.css", array(), '5.8.2' );
    wp_enqueue_style( 'carousel-css', get_template_directory_uri() . '/assets/css/owl.carousel.min.css', array(), '4.5.0' );
    wp_enqueue_style( 'owl-default-css', get_template_directory_uri() . '/assets/css/owl.theme.default.min.css', array(), '4.5.0' );


   
   
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), '3.3.1',true ); 
    wp_register_script( 'slim', "https://code.jquery.com/jquery-3.3.1.slim.min.js", array(), '3.3.1' ,true);  
    wp_enqueue_script( 'popper', "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js", array(), '1.14.7' ,true);  
    wp_enqueue_script( 'bootstrap-js', "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js", array(), '4.3.1',true );  
     
    wp_enqueue_script( 'wow-js','https://unpkg.com/aos@next/dist/aos.js', array(), '5.8.1', true );    
    wp_enqueue_script( 'owl.min', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array(), '1.0.0', true );   
    wp_enqueue_script( 'script', get_template_directory_uri() . '/assets/js/script.js' , array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'road9_scripts' );

function admin_scripts($hook) {    
    if ( 'edit.php' != $hook && $hook != 'post.php' && $hook != 'post-new.php') {
        return;
    }
    wp_enqueue_script( 'my_admin_script', get_template_directory_uri() . '/js/admin.js' );
}
add_action( 'admin_enqueue_scripts', 'admin_scripts' );

add_action( 'after_setup_theme', 'my_theme_setup' );
function my_theme_setup(){
  load_theme_textdomain( 'road9', get_template_directory() . '/languages' );
}

////////////// breadcrumb ////////////////
function breadcrumbs() {  	

    $showOnHome = 0; // 1 - show "breadcrumbs" on home page, 0 - hide  
    $delimiter = ' / '; // divider  
    $home = 'HOME'; // text for link "Home"  
    $showCurrent = 1; // 1 - show title current post/page, 0 - hide  
    $before = '<span class="page-active">'; // open tag for active breadcrumb  
    $after = '</span>'; // close tag for active breadcrumb 
  
    global $post;  
    $homeLink = home_url();  
    if (is_front_page()) {  
      if ($showOnHome == 1) echo '<h6><a href="' . $homeLink . '">' .__('HOME', 'road9') . '</a></h6>';
    
    } else {  
      echo '<h6><a href="' . $homeLink . '">' .__('HOME', 'road9') . '</a> ' . $delimiter . ' ';
    
      if ( is_home() ) {
  
          echo $before . 'Blog' . $after;
  
      } elseif ( is_category() ) {
  
        $thisCat = get_category(get_query_var('cat'), false);
  
        if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
  
        echo $before  . single_cat_title('', false)  . $after;
   
      } elseif ( is_search() ) {
          echo $before . 'Search for: "' . get_search_query() . '"' . $after;
    
      } elseif ( is_day() ) {
  
        echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></h6> ' . $delimiter . ' ';
  
        echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a></h6> ' . $delimiter . ' ';
  
        echo $before . get_the_time('d') . $after;
    
      } elseif ( is_month() ) {
  
        echo '<h6><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a></h6> ' . $delimiter . ' ';
  
        echo $before . get_the_time('F') . $after;
    
      } elseif ( is_year() ) {
  
        echo $before . get_the_time('Y') . $after;
    
  
      } elseif ( is_single() && !is_attachment() ) {
  
        if ( get_post_type() != 'post' ) {
  
            $post_name = get_post_type();
  
          $post_type = get_post_type_object(get_post_type());
  
          $slug = $post_type->rewrite;
  
          echo '<a href="' . $homeLink . '/' . $post_name . '/">' . $post_type->labels->menu_name . '</a>';
  
          if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
  
        } else {
  
          $cat = get_the_category(); $cat = $cat[0];
  
          $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
  
          if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
  
          //echo $cats;
           $title = get_the_title();
            $title = explode(' ', $title, 10);
            array_pop($title);
            $title = implode(' ', $title) . ' &hellip;';
  
          if ($showCurrent == 1) echo $cats .  $before . $title . $after;
  
        } 
  
  
      } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
  
        $post_type = get_post_type_object(get_post_type());
  
        echo $before . $post_type->labels->singular_name . $after;
   
  
      } elseif ( is_attachment() ) {
  
        $parent = get_post($post->post_parent);
  
        $cat = get_the_category($parent->ID); $cat = $cat[0];
  
        //echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
  
        echo '<h6><a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a></h6>';
  
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
   
  
      } elseif ( is_page() && !$post->post_parent ) {
  
        if ($showCurrent == 1) echo $before . get_the_title() . $after;
  
    
      } elseif ( is_page() && $post->post_parent ) {
  
        $parent_id  = $post->post_parent;
  
        $breadcrumbs = array();
  
        while ($parent_id) {
  
          $page = get_page($parent_id);
  
          $breadcrumbs[] = '<h6><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></h6>';
  
          $parent_id  = $page->post_parent;
  
        }
  
        $breadcrumbs = array_reverse($breadcrumbs);
  
        for ($i = 0; $i < count($breadcrumbs); $i++) {
  
          echo $breadcrumbs[$i];
  
          if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
  
        }
  
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
  
    
      } elseif ( is_tag() ) {
  
        echo $before . 'Tag Archives: "' . single_tag_title('', false) . '"' . $after;
    
      } elseif ( is_author() ) {
  
        global $author;
  
        $userdata = get_userdata($author);
  
        echo $before . 'by ' . $userdata->display_name . $after;
  
  
      } elseif ( is_404() ) {
  
        echo $before . '404' . $after;
  
      }   
   
    }     
  
}

function core_paging_nav() {
  // Don't print empty markup if there's only one page.
  if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}
	$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$query_args   = array();
	$url_parts    = explode( '?', $pagenum_link );
	if ( isset( $url_parts[1] ) ) {
		wp_parse_str( $url_parts[1], $query_args );
	}
	$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
	$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';
	$format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';

	$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';

	// Set up paginated links.

	$links = paginate_links( array(
		'base'     => $pagenum_link,
		'format'   => $format,
		'total'    => $GLOBALS['wp_query']->max_num_pages,
		'is-current'  => $paged,
		'mid_size' => 1,	
		'prev_text' => __( ' <div class="prev px-3">
    <span>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.172 10.889">
        <defs>
          <style>
            .cls-1 {
              fill: #CECECE;
            }
          </style>
        </defs>
        <path id="right-arrow" class="cls-1" d="M13.114,4.838a.549.549,0,1,0-.78.773l3.957,3.957H-2.454A.544.544,0,0,0-3,10.115a.55.55,0,0,0,.546.554H16.291l-3.957,3.95a.56.56,0,0,0,0,.78.547.547,0,0,0,.78,0l4.894-4.894a.537.537,0,0,0,0-.773Z" transform="translate(3 -4.674)"/>
      </svg>
    </span>
    <p>Previous</p>
  </div>', 'road9' ),
		'next_text' => __( '<div class="next px-3">
         
    <span>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.172 10.889">
          <defs>
            <style>
              .cls-2 {
                fill: #187EC1;
              }
            </style>
          </defs>
          <path id="right-arrow" class="cls-2" d="M13.114,4.838a.549.549,0,1,0-.78.773l3.957,3.957H-2.454A.544.544,0,0,0-3,10.115a.55.55,0,0,0,.546.554H16.291l-3.957,3.95a.56.56,0,0,0,0,.78.547.547,0,0,0,.78,0l4.894-4.894a.537.537,0,0,0,0-.773Z" transform="translate(3 -4.674)"/>
        </svg>
    </span><p>Next</p>
  </div>', 'road9' ),

	) );



	if ( $links ) :
	?>

<section id="pagination" class="pt-2">

			<?php echo $links; ?>

</section>

	<?php

	endif;

}

function custom_posts_per_page( $query ) {
    if(is_category( 'event' ) || is_category('news')|| is_category('الأحداث') || is_category('الأخبار')){

      $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
      set_query_var('posts_per_page', 1);
      set_query_var('paged', $paged);  
    }
		
	
}
add_action( 'pre_get_posts', 'custom_posts_per_page' );


function wpdocs_theme_name_wp_title( $title,$sep ) {
    if ( is_feed() ) {
        return $title;
    }
     
    global $page, $paged;
    $sep = '|';
 
    // Add the blog description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( is_home() || is_front_page() )  {
        $title .= get_bloginfo( 'name' );
        $title .= " $sep $site_description";
    }else{
        $title .= ' '. $sep .' '. get_bloginfo( 'name' );
    }
 
    // Add a page number if necessary:
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
        $title .= " $sep " . sprintf( __( 'Page %s', '_s' ), max( $paged, $page ) );
    }
    return $title;
}
add_filter( 'wp_title', 'wpdocs_theme_name_wp_title', 10, 2 );


add_action( 'wpcf7_init', 'custom_add_form_tag_specilaity' );
 
function custom_add_form_tag_specilaity() {
    wpcf7_add_form_tag( 'speciality', 'custom_speciality_form_tag_handler' ); // "speciality" is the type of the form-tag
}

function custom_speciality_form_tag_handler(){

    $args = array(
        'post_type' => 'speciality',
        'posts_per_page' => -1,
        'orderby' => 'ID',
        'order' => 'ASC',        
    );
    $query = new WP_Query( $args );
    $specialities = array();
    while ( $query->have_posts() ) :
        $query->the_post();                       
        $specialities[] = get_the_ID();
                        

    endwhile; // End of the loop.
    wp_reset_postdata();
    
    $print = '<select name="speciality"><option>Select Speciality</option>';

    foreach ($specialities as $speciality) {
      if($_GET['selected-speciality'] == get_the_title($speciality)){
        $selected = 'selected';
      }else{
        $selected ='';
      }
      $print .='<option value='.get_the_title($speciality).' '.$selected .'>'.get_the_title($speciality).'</option>';
    }

    $print .='</select>';

    return $print;



}

function theme_header_metadata() {

    // Post object if needed
     global $post;
$post_id = $post->ID;
$post_title = get_the_title( $post_id );
//$post_desc = get_the_content( $post_id );
$post_desc = $post->post_content;
$post_desc = strip_tags($post_desc) ;
$post_desc = preg_replace("/[^a-zA-Z0-9\s]/", "", $post_desc );
$mage = get_the_post_thumbnail_url($post_id);
    // Page conditional if needed
    // if( is_page() ){}

  ?>

<meta property="og:title" content="<?php echo $post_title;?>"/>
<meta property="og:image" content="<?php echo $mage;?>"/>
<meta property="og:site_name" content="Planet Medical Center"/>
<meta property="og:description" content="<?php echo $post_desc;?>"/>
    

  <?php

}
add_action( 'wp_head', 'theme_header_metadata' );