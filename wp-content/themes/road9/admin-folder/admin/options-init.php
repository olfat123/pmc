<?php

    /**
     * For full documentation, please visit: http://docs.reduxframework.com/
     * For a more extensive sample-config file, you may look at:
     * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "road9";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        'opt_name' => 'road9',
        'dev_mode' => false,
        'use_cdn' => TRUE,
        'display_name' => 'Road9 Options',
        'display_version' => '1.0.0',
        'page_slug' => 'road9-options',
        'page_title' => 'Road9 Options',
        'update_notice' => TRUE,
        'admin_bar' => TRUE,
        'menu_type' => 'menu',
        'menu_title' => 'Road9 Options',
        'allow_sub_menu' => TRUE,
        'page_parent_post_type' => 'your_post_type',
        'customizer' => TRUE,
        'default_mark' => '*',
        'hints' => array(
            'icon_position' => 'right',
            'icon_size' => 'normal',
            'tip_style' => array(
                'color' => 'light',
            ),
            'tip_position' => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect' => array(
                'show' => array(
                    'duration' => '500',
                    'event' => 'mouseover',
                ),
                'hide' => array(
                    'duration' => '500',
                    'event' => 'mouseleave unfocus',
                ),
            ),
        ),
        'output' => TRUE,
        'output_tag' => TRUE,
        'settings_api' => TRUE,
        'cdn_check_time' => '1440',
        'compiler' => TRUE,
        'page_permissions' => 'manage_options',
        'save_defaults' => TRUE,
        'show_import_export' => TRUE,
        'database' => 'options',
        'transient_time' => '3600',
        'network_sites' => TRUE,
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/reduxframework',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://www.linkedin.com/company/redux-framework',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'admin_folder' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'admin_folder' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'admin_folder' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'admin_folder' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'admin_folder' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    Redux::setSection( $opt_name, array(
        'title'  => __( 'General', 'Road9' ),
        'id'     => 'basic',
        'desc'   => __( 'Basic field with no subsections.', 'Road9' ),
        'icon'   => 'el el-home',
        'fields' => array(
          
            array(
                'id'       => 'website-logo',
                'type'     => 'media', 
                'url'      => true,
                'title'    => __('Logo', 'Road9'),
                'desc'     => __('Basic media uploader with disabled URL input field.', 'Road9'),
                'subtitle' => __('Upload any media using the WordPress native uploader', 'Road9'),
                'default'  => array(
                    'url'=>'https://road9media.com/images/headers/company-logo.png'
                ),
            ),
            

           
        )
     ) );

    
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Header', 'Road9' ),
        'id'     => 'header',
        'desc'   => __( 'Basic field with no subsections.', 'Road9' ),
        'icon'   => 'el el-pencil',
        )
    );

  

    Redux::setSection( $opt_name, array(
        'title'  => __( 'Footer', 'Road9' ),
        'id'     => 'footer',
        'desc'   => __( 'Basic field with no subsections.', 'Road9' ),
        'icon'   => 'el el-cog',
        'fields'     => array(
            array(
                'id'       => 'facebook_link',
                'type'     => 'text',
                'title'    => __( 'Facebook', 'Road9' ),
                'subtitle' => __( 'Subtitle', 'Road9' ),
                'desc'     => __( 'Company Facebook Link', 'Road9' ),
                
            ),
            
            array(
                'id'       => 'twitter_link',
                'type'     => 'text',
                'title'    => __( 'Twitter', 'Road9' ),
                'subtitle' => __( 'Subtitle', 'Road9' ),
                'desc'     => __( 'Company Twitter Link', 'Road9' ),
                
            ),
            array(
                'id'       => 'youtube_link',
                'type'     => 'text',
                'title'    => __( 'Youtube', 'Road9' ),
                'subtitle' => __( 'Subtitle', 'Road9' ),
                'desc'     => __( 'Company Youtube Link', 'Road9' ),
                
            ),
            array(
                'id'       => 'instagram_link',
                'type'     => 'text',
                'title'    => __( 'Instagram', 'Road9' ),
                'subtitle' => __( 'Subtitle', 'Road9' ),
                'desc'     => __( 'Company Instagram Link', 'Road9' ),
                
            ),
        )
        )
    );
    ///////////////////Home Page ////////////////////////////////
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Home', 'Road9' ),
        'id'     => 'home',
        'desc'   => __( 'Basic field with no subsections.', 'Road9' ),
        'icon'   => 'el el-home'
    ));
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Home English', 'Road9' ),
        'id'     => 'home-english-section',
        'desc'   => __( 'Basic field with no subsections.', 'Road9' ),        
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'home-banner',
                'type'     => 'media', 
                'url'      => true,
                'title'    => __('Home Banner Image', 'Road9'),
                'desc'     => __('Basic media uploader with disabled URL input field.', 'Road9'),
                'subtitle' => __('Upload any media using the WordPress native uploader', 'Road9'),
                'default'  => array(
                    'url'=>'https://road9media.com/images/headers/company-logo.png'
                ),
            ),            
            array(
                'id'       => 'banner-title',
                'type'     => 'text',
                'title'    => __( 'Banner Title', 'Road9' ),
                'subtitle' => __( 'Subtitle', 'Road9' ),
                'desc'     => __( '', 'Road9' ),
                
            ),            
            array(
                'id'       => 'button-text',
                'type'     => 'text',
                'title'    => __( 'Button Text', 'Road9' ),
                'subtitle' => __( 'Subtitle', 'Road9' ),
                'desc'     => __( '', 'Road9' ),
                
            ),
            array(
                'id'       => 'button-icon',
                'type'     => 'media', 
                'url'      => true,
                'title'    => __('Button Icon Image', 'Road9'),
                'desc'     => __('Basic media uploader with disabled URL input field.', 'Road9'),
                'subtitle' => __('Upload any media using the WordPress native uploader', 'Road9'),
                'default'  => array(
                    'url'=>'https://road9media.com/images/headers/company-logo.png'
                ),
            ), 
            array(
                'id'       => 'button-link',
                'type'     => 'text',
                'title'    => __( 'Button Link', 'Road9' ),
                'subtitle' => __( 'Subtitle', 'Road9' ),
                'desc'     => __( '', 'Road9' ),
                
            ),
            array(
                'id' => 'first-home-section-start',
                'type' => 'section',
                'title' => __('First Section', 'redux-framework-demo'),
                'indent' => true 
            ),
            array(
                'id'       => 'first-section-title',
                'type'     => 'text',
                'title'    => __( 'What We Provide', 'Road9' ),
                'subtitle' => __( 'Subtitle', 'Road9' ),
                'desc'     => __( '', 'Road9' ),
                
            ),
            array(
                'id'       => 'first-section-description',
                'type'     => 'textarea',
                'title'    => __( 'First Section Description', 'Road9' ),
                'subtitle' => __( 'Description', 'Road9' ),
                'desc'     => __( 'The Company Description', 'Road9' ),
                'default'  => 'Default Text',
            ),
            array(
                'id'       => 'medical-service-icon',
                'type'     => 'media', 
                'url'      => true,
                'title'    => __('Medical Services Icon', 'Road9'),
                'desc'     => __('Basic media uploader with disabled URL input field.', 'Road9'),
                'subtitle' => __('Upload any media using the WordPress native uploader', 'Road9'),
                'default'  => array(
                    'url'=>''
                ),
            ),            
            array(
                'id'       => 'beauty-service-icon',
                'type'     => 'media', 
                'url'      => true,
                'title'    => __('Beauty Services Icon', 'Road9'),
                'desc'     => __('Basic media uploader with disabled URL input field.', 'Road9'),
                'subtitle' => __('Upload any media using the WordPress native uploader', 'Road9'),
                'default'  => array(
                    'url'=>''
                ),
            ),
            array(
                'id'     => 'first-home-section-end',
                'type'   => 'section',
                'indent' => false,
            ),
            array(
                'id' => 'second-home-section-start',
                'type' => 'section',
                'title' => __('Second Section', 'redux-framework-demo'),
                'indent' => true 
            ),
            array(
                'id'       => 'second-section-title',
                'type'     => 'text',
                'title'    => __( 'Second Section Title', 'Road9' ),
                'subtitle' => __( 'Subtitle', 'Road9' ),
                'desc'     => __( 'Doctors', 'Road9' ),
                
            ),
            array(
                'id'       => 'second-section-subtitle',
                'type'     => 'text',
                'title'    => __( 'Second Section Sub Title', 'Road9' ),
                'subtitle' => __( 'Subtitle', 'Road9' ),
                'desc'     => __( 'well picked', 'Road9' ),
                
            ),
            array(
                'id'       => 'second-section-description',
                'type'     => 'textarea',
                'title'    => __( 'second Section Description', 'Road9' ),
                'subtitle' => __( 'Description', 'Road9' ),
                'desc'     => __( 'The Company Description', 'Road9' ),
                'default'  => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque explicabo neque facere harum, quas sed ad, rerum commodi tempora, earum odit aspernatur? Omnis sint minima obcaecati distinctio debitis vitae nobis!',
            ),
            
            array(
                'id'     => 'second-home-section-end',
                'type'   => 'section',
                'indent' => false,
            ),

            array(
                'id' => 'third-home-section-start',
                'type' => 'section',
                'title' => __('third Section', 'redux-framework-demo'),
                'indent' => true 
            ),
            array(
                'id'       => 'third-section-title',
                'type'     => 'text',
                'title'    => __( 'third Section Title', 'Road9' ),
                'subtitle' => __( 'Subtitle', 'Road9' ),
                'desc'     => __( 'OUR LATEST', 'Road9' ),
                
            ),
            array(
                'id'       => 'third-section-subtitle',
                'type'     => 'text',
                'title'    => __( 'third Section Sub Title', 'Road9' ),
                'subtitle' => __( 'Subtitle', 'Road9' ),
                'desc'     => __( 'OFFERS', 'Road9' ),
                
            ),
            array(
                'id'       => 'third-section-description',
                'type'     => 'textarea',
                'title'    => __( 'third Section Description', 'Road9' ),
                'subtitle' => __( 'Description', 'Road9' ),
                'desc'     => __( 'The Company Description', 'Road9' ),
                'default'  => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque explicabo neque facere harum, quas sed ad, rerum commodi tempora, earum odit aspernatur? Omnis sint minima obcaecati distinctio debitis vitae nobis!',
            ),
            
            array(
                'id'     => 'third-home-section-end',
                'type'   => 'section',
                'indent' => false,
            ),
            
        )
    ));
   

    ///////////////// About us ////////////////////////////////////
    Redux::setSection( $opt_name, array(
        'title'  => __( 'About us', 'Road9' ),
        'id'     => 'about-us',
        'desc'   => __( 'Basic field with no subsections.', 'Road9' ),
        'icon'   => 'el el-book',
        )
    );
    Redux::setSection( $opt_name, array(
        'title'  => __( 'About Us', 'Road9' ),
        'id'     => 'about-section',
        'desc'   => __( 'Basic field with no subsections.', 'Road9' ),        
        'subsection' => true,
        'fields'     => array(
            array(
                'id' => 'first-section-start',
                'type' => 'section',
                'title' => __('First Section', 'redux-framework-demo'),
                'indent' => true 
            ),
            array(
                'id'       => 'about-first-section-title',
                'type'     => 'text',
                'title'    => __( 'First Section Title', 'Road9' ),                
                'desc'     => __( 'About us Page first section title', 'Road9' ),
                'default'  => 'PLANET MEDICAL CENTERL',
            ),
            array(
                'id'       => 'about-first-section-description',
                'type'     => 'textarea',
                'title'    => __( 'First Section Description', 'Road9' ),
                'subtitle' => __( 'Description', 'Road9' ),
                'desc'     => __( 'The Company Description', 'Road9' ),
                'default'  => 'Planet Medical Center is a private Polyclinic in Qatar, established in 2016, with a new vision in
                medical care.
                We have a unique set up with multi-specialty clinics run by Senior Consultants.',
            ),
            array(
                'id'       => 'about-first-section-image',
                'type'     => 'media',
                'url'      => true,
                'title'    => __('First Section Image', 'Road9'),
                'desc'     => __('Basic media uploader with disabled URL input field.', 'Road9'),
                'subtitle' => __('Upload any media using the WordPress native uploader', 'Road9'),
                'default'  => array(
                'url'      => 'https://road9media.com/images/headers/company-logo.png'
                ),
            ),
            array(
                'id'     => 'first-section-end',
                'type'   => 'section',
                'indent' => false,
            ),
            array(
                'id' => 'second-section-start',
                'type' => 'section',
                'title' => __('Second Section', 'redux-framework-demo'),
                'indent' => true 
            ),
            array(
                'id'       => 'about-second-section-title',
                'type'     => 'text',
                'title'    => __( 'Second Section Title', 'Road9' ),                
                'desc'     => __( 'About us Page second section title', 'Road9' ),
                'default'  => 'The Laboratory and the Radiology',
            ),
            array(
                'id'       => 'about-second-section-description',
                'type'     => 'textarea',
                'title'    => __( 'Second Section Description', 'Road9' ),
                'subtitle' => __( 'Description', 'Road9' ),
                'desc'     => __( 'The Company Description', 'Road9' ),
                'default'  => 'departments have the latest equipment that will
                ensure the highest degree of accuracy and safety.',
            ),
            array(
                'id'       => 'about-second-section-image',
                'type'     => 'media',
                'url'      => true,
                'title'    => __('Second Section Image', 'Road9'),
                'desc'     => __('Basic media uploader with disabled URL input field.', 'Road9'),
                'subtitle' => __('Upload any media using the WordPress native uploader', 'Road9'),
                'default'  => array(
                'url'      => 'https://road9media.com/images/headers/company-logo.png'
                ),
            ),
            array(
                'id'     => 'second-section-end',
                'type'   => 'section',
                'indent' => false,
            ),
            array(
                'id' => 'third-section-start',
                'type' => 'section',
                'title' => __('Third Section', 'redux-framework-demo'),
                'indent' => true 
            ),
            array(
                'id'       => 'about-third-section-title',
                'type'     => 'text',
                'title'    => __( 'Third Section Title', 'Road9' ),                
                'desc'     => __( 'About us Page third section title', 'Road9' ),
                'default'  => 'A comprehensive well-stocked Pharmacy',
            ),
            array(
                'id'       => 'about-third-section-description',
                'type'     => 'textarea',
                'title'    => __( 'Third Section Description', 'Road9' ),
                'subtitle' => __( 'Description', 'Road9' ),
                'desc'     => __( 'The Company Description', 'Road9' ),
                'default'  => 'is also available for the convenience of
                the patients.',
            ),
            array(
                'id'       => 'about-third-section-image',
                'type'     => 'media',
                'url'      => true,
                'title'    => __('Third Section Image', 'Road9'),
                'desc'     => __('Basic media uploader with disabled URL input field.', 'Road9'),
                'subtitle' => __('Upload any media using the WordPress native uploader', 'Road9'),
                'default'  => array(
                'url'      => 'https://road9media.com/images/headers/company-logo.png'
                ),
            ),
            array(
                'id'     => 'third-section-end',
                'type'   => 'section',
                'indent' => false,
            ),
            array(
                'id' => 'last-section-start',
                'type' => 'section',
                'title' => __('Last Section', 'redux-framework-demo'),
                'indent' => true 
            ),
            array(
                'id'       => 'company-vision',
                'type'     => 'textarea',
                'title'    => __( 'Company Vision', 'Road9' ),
                'subtitle' => __( 'Vision', 'Road9' ),
                'desc'     => __( 'The Company Description', 'Road9' ),
                'default'  => 'To be recognized as a center of excellence in Qatar.',
            ),
            array(
                'id'       => 'company-mission',
                'type'     => 'textarea',
                'title'    => __( 'Company Mission', 'Road9' ),
                'subtitle' => __( 'Mission', 'Road9' ),
                'desc'     => __( 'The Company Description', 'Road9' ),
                'default'  => 'We shall treat each patient like a member of our own family to promote, enhance and facilitate
                the health and wellbeing of the people in Qatar and to provide quality healthcare at affordable
                cost.',
            ),
            array(
                'id'       => 'last-paragraph-title',
                'type'     => 'text',
                'title'    => __( 'Last Paragraph Title', 'Road9' ),                
                'desc'     => __( '', 'Road9' ),
                'default'  => 'Corporate Social Responsibilities',
            ),
            array(
                'id'       => 'last-paragraph-description',
                'type'     => 'textarea',
                'title'    => __( 'Last Paragraph Description', 'Road9' ),
                'subtitle' => __( 'Description', 'Road9' ),
                'desc'     => __( 'The Company Description', 'Road9' ),
                'default'  => 'Planet Medical Center is proud to be a frequent contributor to the local community. It is our
                aim to maintain good Corporate Social Responsibility by frequently giving back through
                awareness campaigns, charity sponsorship, event support, and providing educational or
                medical services to various societies, charities, or clubs that are in need.',
            ),
            array(
                'id'       => 'last-section-image',
                'type'     => 'media',
                'url'      => true,
                'title'    => __('Last Section Image', 'Road9'),
                'desc'     => __('Basic media uploader with disabled URL input field.', 'Road9'),
                'subtitle' => __('Upload any media using the WordPress native uploader', 'Road9'),
                'default'  => array(
                    'url'=>'https://road9media.com/images/headers/company-logo.png'
                ),
            ),
            array(
                'id'     => 'last-section-end',
                'type'   => 'section',
                'indent' => false,
            ),           
            
        )
    ) );

    Redux::setSection( $opt_name, array(
        'title'  => __( 'About Us Arabic', 'Road9' ),
        'id'     => 'about-section-arabic',
        'desc'   => __( 'Basic field with no subsections.', 'Road9' ),        
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'about-first-section-title-arabic',
                'type'     => 'text',
                'title'    => __( 'First Section Arabic Title', 'Road9' ),                
                'desc'     => __( 'About us Page first section title', 'Road9' ),
                'default'  => 'مركز بلانت الطبي',
            ),
            array(
                'id'       => 'about-first-section-description-arabic',
                'type'     => 'textarea',
                'title'    => __( 'First Section Arabic Description', 'Road9' ),
                'subtitle' => __( 'Description', 'Road9' ),
                'desc'     => __( 'The Company Description', 'Road9' ),
                'default'  => 'مركز بلانت الطبي هو أحد أفضل المركز الطبية في قطر، تأسس عام 2016، برؤية جديدة في القطاع الصحي.
                يعمل بالمركز مجموعة متميزة من الأطباء الأخصائيين بتخصصات طبية مختلفة.',
            ),
            
            array(
                'id'       => 'about-second-section-title-arabic',
                'type'     => 'text',
                'title'    => __( 'Second Section Title', 'Road9' ),                
                'desc'     => __( 'About us Page second section title', 'Road9' ),
                'default'  => 'قسم الأشعة والمختبر',
            ),
            array(
                'id'       => 'about-second-section-description-arabic',
                'type'     => 'textarea',
                'title'    => __( 'Second Section Description', 'Road9' ),
                'subtitle' => __( 'Description', 'Road9' ),
                'desc'     => __( 'The Company Description', 'Road9' ),
                'default'  => 'يحوي على أحدث الأجهزة الطبية، لضمان دقة النتائج والأمان للمرضى ',
            ),
           
            array(
                'id'       => 'about-third-section-title-arabic',
                'type'     => 'text',
                'title'    => __( 'Third Section Title', 'Road9' ),                
                'desc'     => __( 'About us Page third section title', 'Road9' ),
                'default'  => 'صيدلية مركز بلانت الطبي',
            ),
            array(
                'id'       => 'about-third-section-description-arabic',
                'type'     => 'textarea',
                'title'    => __( 'Third Section Description', 'Road9' ),
                'subtitle' => __( 'Description', 'Road9' ),
                'desc'     => __( 'The Company Description', 'Road9' ),
                'default'  => 'تحوي على كل ماتحتاجونه من الأدوية والمستلزمات.',
            ),
            
            array(
                'id'       => 'company-vision-arabic',
                'type'     => 'textarea',
                'title'    => __( 'Company Vision', 'Road9' ),
                'subtitle' => __( 'Vision', 'Road9' ),
                'desc'     => __( 'The Company Description', 'Road9' ),
                'default'  => 'الوصول الى الريادة من خلال تقديم حلول الرعاية الطبية وخدماتها المسانده بجوده عالية وقيمه مناسبه على مستوى دولة قطر.',
            ),
            array(
                'id'       => 'company-mission-arabic',
                'type'     => 'textarea',
                'title'    => __( 'Company Mission', 'Road9' ),
                'subtitle' => __( 'Mission', 'Road9' ),
                'desc'     => __( 'The Company Description', 'Road9' ),
                'default'  => 'معاملة كل مريض كفرد من عائلتنا، وتوفير الخدمة الطبية بأعلى معايير الجودة، وبتكلفة مناسبة.',
            ),
            array(
                'id'       => 'last-paragraph-title-arabic',
                'type'     => 'text',
                'title'    => __( 'Last Paragraph Title', 'Road9' ),                
                'desc'     => __( '', 'Road9' ),
                'default'  => 'الدور المجتمعي',
            ),
            array(
                'id'       => 'last-paragraph-description-arabic',
                'type'     => 'textarea',
                'title'    => __( 'Last Paragraph Description', 'Road9' ),
                'subtitle' => __( 'Description', 'Road9' ),
                'desc'     => __( 'The Company Description', 'Road9' ),
                'default'  => 'يسعدنا أن نكون أحد المتطوعين لخدمة المجتمع بقطر. من خلال تقديم حملات التوعية ودعم للمؤسسات الخيرية والأشخاص المحتاجين',
            ),
                        
        )
    ) );
    ///////////////// Contact Us /////////////////

    Redux::setSection( $opt_name, array(
        'title'  => __( 'Contact us', 'Road9' ),
        'id'     => 'contact-us',
        'desc'   => __( 'Basic field with no subsections.', 'Road9' ),
        'icon'   => 'el el-book',
        'fields'     => array(
            array(
                'id' => 'contact-section-start',
                'type' => 'section',
                'title' => __('Contact Info', 'redux-framework-demo'),
                'indent' => true 
            ),
            array(
                'id'       => 'contact-image',
                'type'     => 'media',
                'url'      => true,
                'title'    => __('Contact Us Image', 'Road9'),
                'desc'     => __('Basic media uploader with disabled URL input field.', 'Road9'),
                'subtitle' => __('Upload any media using the WordPress native uploader', 'Road9'),
                'default'  => array(
                    'url'=>'https://road9media.com/images/headers/company-logo.png'
                ),
            ),
            array(
                'id'       => 'address-textarea',
                'type'     => 'textarea',
                'title'    => __( 'Address', 'Road9' ),
                'subtitle' => __( 'Company Address', 'Road9' ),
                'desc'     => __( 'Field Description', 'Road9' ),
                'default'  => 'Default Text',
            ),
            array(
                'id'       => 'address-textarea-arabic',
                'type'     => 'textarea',
                'title'    => __( 'Address in Arabic', 'Road9' ),
                'subtitle' => __( 'Company Address', 'Road9' ),
                'desc'     => __( 'Field Description', 'Road9' ),
                'default'  => 'Default Text',
            ),
            array(
                'id'       => 'phone-text',
                'type'     => 'text',
                'title'    => __( 'Phone', 'Road9' ),
                'subtitle' => __( 'Company Phone', 'Road9' ),
                'desc'     => __( 'Field Description', 'Road9' ),
                'default'  => 'Default Text',
            ),
            array(
                'id'     => 'contact-section-end',
                'type'   => 'section',
                'indent' => false,
            ), 
            array(
                'id' => 'working-section-start',
                'type' => 'section',
                'title' => __('Working Hours', 'redux-framework-demo'),
                'indent' => true 
            ),
            array(
                'id'       => 'monday',
                'type'     => 'text',
                'title'    => __( 'Monday', 'Road9' ),
                'subtitle' => __( 'Monday Working Hours', 'Road9' ),
                'desc'     => __( 'Monday Working Hours', 'Road9' ),
                'default'  => '07:00 am - 12:00 am',
            ),
            array(
                'id'       => 'tuesday',
                'type'     => 'text',
                'title'    => __( 'Tuesday', 'Road9' ),
                'subtitle' => __( 'Tuesday Working Hours', 'Road9' ),
                'desc'     => __( 'Tuesday Working Hours', 'Road9' ),
                'default'  => '07:00 am - 12:00 am',
            ),
            array(
                'id'       => 'wednesday',
                'type'     => 'text',
                'title'    => __( 'Wednesday', 'Road9' ),
                'subtitle' => __( 'Wednesday Working Hours', 'Road9' ),
                'desc'     => __( 'Wednesday Working Hours', 'Road9' ),
                'default'  => '07:00 am - 12:00 am',
            ),
            array(
                'id'       => 'thursday',
                'type'     => 'text',
                'title'    => __( 'Thursday', 'Road9' ),
                'subtitle' => __( 'Thursday Working Hours', 'Road9' ),
                'desc'     => __( 'Thursday Working Hours', 'Road9' ),
                'default'  => '07:00 am - 12:00 amt',
            ),
            array(
                'id'       => 'friday',
                'type'     => 'text',
                'title'    => __( 'Friday', 'Road9' ),
                'subtitle' => __( 'Friday Working Hours', 'Road9' ),
                'desc'     => __( 'Friday Working Hours', 'Road9' ),
                'default'  => '07:00 am - 12:00 am',
            ),
            array(
                'id'       => 'saturday',
                'type'     => 'text',
                'title'    => __( 'Saturday', 'Road9' ),
                'subtitle' => __( 'Saturday Working Hours', 'Road9' ),
                'desc'     => __( 'Saturday Working Hours', 'Road9' ),
                'default'  => '07:00 am - 12:00 am',
            ),
            array(
                'id'       => 'sunday',
                'type'     => 'text',
                'title'    => __( 'Sunday', 'Road9' ),
                'subtitle' => __( 'Sunday Working Hours', 'Road9' ),
                'desc'     => __( 'Sunday Working Hours', 'Road9' ),
                'default'  => '07:00 am - 12:00 am',
            ),
            array(
                'id'     => 'working-section-end',
                'type'   => 'section',
                'indent' => false,
            ), 
        )
        )
    );
    
///////////////////////// All Features Page ///////////////////
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Specialities', 'Road9' ),
        'id'     => 'specialities',
        'desc'   => __( 'Basic field with no subsections.', 'Road9' ),
        'icon'   => 'el el-book',
        'fields'     => array(
            array(
                'id'       => 'specialities-image',
                'type'     => 'media',
                'url'      => true,
                'title'    => __('Specialities Image', 'Road9'),
                'desc'     => __('Basic media uploader with disabled URL input field.', 'Road9'),
                'subtitle' => __('Upload any media using the WordPress native uploader', 'Road9'),
                'default'  => array(
                    'url'=>'https://road9media.com/images/headers/company-logo.png'
                ),
            ),
        )
        )
    );
    
///////////////////////// Careers Page ///////////////////
    Redux::setSection( $opt_name, array(
        'title'  => __( 'Careers', 'Road9' ),
        'id'     => 'careers',
        'desc'   => __( 'Basic field with no subsections.', 'Road9' ),
        'icon'   => 'el el-book',
        'fields'     => array(
            array(
                'id'       => 'careers-title',
                'type'     => 'text',
                'title'    => __( 'Careers Title', 'Road9' ),                
                'desc'     => __( 'Careers Title', 'Road9' ),
                'default'  => 'Join our Team',
            ),
            array(
                'id'       => 'careers-descrption',
                'type'     => 'textarea',
                'title'    => __( 'Careers Description', 'Road9' ),
                'subtitle' => __( 'Description', 'Road9' ),
                'desc'     => __( 'Careers Description', 'Road9' ),
                'default'  => 'Default Text',
            ),

            array(
                'id'       => 'careers-title-arabic',
                'type'     => 'text',
                'title'    => __( 'Careers Title Arabic', 'Road9' ),                
                'desc'     => __( 'Careers Title Arabic', 'Road9' ),
                'default'  => 'انضم الينا',
            ),
            array(
                'id'       => 'careers-descrption-arabic',
                'type'     => 'textarea',
                'title'    => __( 'Careers Description Arabic', 'Road9' ),
                'subtitle' => __( 'Description', 'Road9' ),
                'desc'     => __( 'Careers Description Arabic', 'Road9' ),
                'default'  => 'Default Text',
            ),
            
        )
        )
    );

    /*
     * <--- END SECTIONS
     */
