<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Road9
 * @since 1.0.0
 */

get_header();
?>
<div class="circle-sub" style="z-index: -1"></div>
<div class="container-fluid p-5">
    <?php echo the_title(' <p class="text-uppercase page-sub-title">', '</p>'); ?>  
    <nav class="breadCrumbNav">
        <?php 
        breadcrumbs() ;
        ?>
    </nav>
</div>

<section id="text-section">
    <div class="container">
        <?php 
        
			
        
        /* Start the Loop */
        while ( have_posts() ) :
            the_post();
            the_content();
            //get_template_part( 'template-parts/content/content', 'page' );

            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) {
                comments_template();
            }

        endwhile; // End of the loop.
        ?>

    </div>
</section>
<?php
get_footer();