<?php /* Template Name: Jobs */ 
get_header();
?>
<div class="circle-sub" style="z-index: -1"></div>

<div class="container-fluid p-5">
    <?php echo the_title(' <p class="text-uppercase page-sub-title">', '</p>'); ?>  
    <nav class="breadCrumbNav">
        <?php 
        breadcrumbs() ;
        ?>
    </nav>
</div>

<?php  
  global $road9; 
  if(get_locale()== 'en_GB'){
    $title = $road9['careers-title'];
    $description = $road9['careers-descrption'];
  }else {
    $title = $road9['careers-title-arabic'];
    $description = $road9['careers-descrption-arabic'];
  }            
  
?>

<section id="careers" class="pt-5 form">
    <div class="container py-5">
        <div class="row">
          <div class="col-lg-4">
              <h5 class="text-uppercase h5 font-weight-bold"><?php echo $title; ?></h5>
              
             <p class="small para w-75 pb-5"><?php echo $description; ?></p>
          </div>  

          <div class="col-lg-8">
            <h5 class="text-uppercase h5 font-weight-bold pb-5"><?php echo __( 'application form', 'road9' ) ; ?></h5>
              <?php
              while (have_posts()):the_post() ;
              	the_content();

              endwhile;
              ?>

          </div>

        
    </div>
</section>


  

<?php
get_footer();
?>