<?php /* Template Name: Contact */ 
get_header();
?>
<div class="circle-sub" style="z-index: -1"></div>
<section class="grey-section-full-page"></section>
<div class="container-fluid p-5">
    <?php echo the_title(' <p class="text-uppercase page-sub-title">', '</p>'); ?>  
    <nav class="breadCrumbNav">
        <?php 
        breadcrumbs() ;
        ?>
    </nav>
</div>      
        
            <?php 
            

            global $road9;
            $image = $road9['contact-image']['url'];
            if(get_locale()== 'en_GB'){
              $address = $road9['address-textarea'];
            }else {
              $address = $road9['address-textarea-arabic'];
            }            
            $phone = $road9['phone-text'];
            $monday = $road9['monday'];
            $tuesday = $road9['tuesday'];
            $wednesday = $road9['wednesday'];
            $thursday = $road9['thursday'];
            $friday = $road9['friday'];
            $saturday = $road9['saturday'];
            $sunday = $road9['sunday'];


            ?>
<section id="contact-us">
  
    <section class="grey-section-full-page"></section>
      <!-- alert -->
      <!--<div class="alert alert-dismissible fade show" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
          </button>
          <strong></strong>
      </div>-->
      <!-- alert -->
        <div class="row">
            <div class="col-xl-4 contact-info">  
                <div>
                    <h5 class="h5 font-weight-bold text-uppercase pb-3"><?php echo __( 'contact info', 'road9' ) ; ?></h5>
                    <b class="text-capitalize"><?php echo __( 'address', 'road9' ) ; ?></b>
                    <p class="small para"><?php echo $address; ?></p>
                    <b class="text-capitalize"><?php echo __( 'phone', 'road9' ) ; ?></b>
                    <p class="small para"><?php echo $phone; ?></p>
                </div>

            </div>
    <div class="col-xl-8">
            <img src="<?php echo $image; ?>" alt="" width="677px" height="513px">  
                    <div class="working-hours">
                            <div class="container p-4">
                                <h5 class="h5 font-weight-bold text-uppercase pb-3"><?php echo __( 'working hours', 'road9' ) ; ?></h5> 
                                <div class="row">
                                    <div class="col-md-6">
                                        <b class="text-capitalize"><?php echo __( 'Monday', 'road9' ) ; ?></b>
                                        <p><?php echo $monday; ?></p>
                                        <b class="text-capitalize"><?php echo __( 'Tuesday', 'road9' ) ; ?></b>
                                        <p><?php echo $tuesday; ?></p>
                                        <b class="text-capitalize"><?php echo __( 'Wednesday', 'road9' ) ; ?></b>
                                        <p><?php echo $wednesday; ?></p>
                                        <b class="text-capitalize"><?php echo __( 'Thursday', 'road9' ) ; ?></b>
                                        <p><?php echo $thursday; ?></p>
                                    </div>
                                    <div class="col-md-6">
                                        <b class="text-capitalize"><?php echo __( 'Friday', 'road9' ) ; ?></b>
                                        <p><?php echo $friday; ?></p>
                                        <b class="text-capitalize"><?php echo __( 'Saturday', 'road9' ) ; ?></b>
                                        <p><?php echo $saturday; ?></p>
                                        <b class="text-capitalize"><?php echo __( 'Sunday', 'road9' ) ; ?></b>
                                        <p><?php echo $sunday; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        </div>


</section>




<!-- ##################### End ################# -->

<!-- ######## Map ######## -->
<section id="map" class="map-contact-us form">
        <iframe class="frame" src="https://snazzymaps.com/embed/161931"></iframe>
        <div class="container">
           <!-- <div class="send-message" data-aos="fade-up" data-aos-duration="2000">
      
                  <div class="container p-5">
                      <div class="h5 text-uppercase font-weight-bold pb-3"><?php echo __( 'Send Us A Message', 'road9' ) ; ?></div>-->
                      <?php
                        while (have_posts()):the_post() ;
                          the_content();

                        endwhile;
                      ?>
                 <!-- </div> 
      
            </div>-->
        </div>
      
      </section>
      <!-- ##### End of Map ####### -->
      

<?php
get_footer();
?>