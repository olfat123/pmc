<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content"> 
 * @package WordPress
 * @subpackage Road9
 * @since 1.0.0
 */

global $road9;
$facebook = $road9['facebook_link'];
$twitter = $road9['twitter_link'];
$youtube = $road9['youtube_link'];
$instagram = $road9['instagram_link'];
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title><?php wp_title($sep = ''); ?></title>
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
	
</head>

<body >
	
	<div id="preloader"></div>
    <div id="body-wrapper">
		<?php 
		
			
			$logo_url = $road9['website-logo']['url']; 
		?>
		<section class="header">
  			<div class="container-fluid d-flex">
				<div class="col-3">
					<a href="<?php echo home_url(); ?>"><img class="m-3 logo" src="<?php echo $logo_url; ?>"  alt="logo" height="75px" width="125px"></a>
				</div>   
    
				<div class="col-9">
					<div class="row w-100 p-0">
					<?php get_search_form() ?>
					<?php foreach(icl_get_languages('skip_missing=0&orderby=code') as $language){
						if(ICL_LANGUAGE_CODE=='en' && $language['language_code'] =='ar'){ ?>
						<p class="d-inline px-3"><a class="arabic px-2" href="<?=$language['url']?>">عربى</a></p>
						<?php  }elseif (ICL_LANGUAGE_CODE=='ar' && $language['language_code'] =='en') { ?>
							<p class="d-inline px-3"><a href="<?=$language['url']?>">EN</a></p>
						<?php }
					}?>
						<div class="hide-nav">
							<div class="social-icons">
								<a href="<?php echo $facebook; ?>" target="_blank"><img class="px-1" src="<?php echo site_url(); ?>/wp-content/uploads/2019/06/facebook-logo.png" alt=""></a>
								<a href="<?php echo $twitter ;?>" target="_blank"><img class="px-1" src="<?php echo site_url(); ?>/wp-content/uploads/2019/06/twitter.png" alt=""></a>
								<a href="<?php echo $youtube ;?>" target="_blank"><img class="px-1" src="<?php echo site_url(); ?>/wp-content/uploads/2019/06/youtube.png" alt=""></a>
								<a href="<?php echo $instagram ;?>" target="_blank"><img class="px-1" src="<?php echo site_url(); ?>/wp-content/uploads/2019/06/instagram.png" alt=""></a>
                      </div>
						</div>
					</div>
			
				<?php
						wp_nav_menu( array(
                                'menu'              => 'top-menu',
                                'theme_location'    => 'top-menu',
                                'depth'             => 5,
                                'container'         => 'nav',
                                'container_class'   => 'navbar navbar-expand hide-nav',
								'menu_class'        => 'navbar-nav first-main',
								'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                'walker'            => new wp_bootstrap_navwalker()
						) 
						); 
				?>
				
			
		</section>
		<!--##### Main Menu #####-->
		<section id="main-menu">
  			<!--##### navbar-expand-lg #####-->
			
				<?php
						wp_nav_menu( array(
                                'menu'              => 'primary',
                                'theme_location'    => 'menu-1',
                                'depth'             => 5,
                                'container'         => 'nav',
                                'container_class'   => 'navbar navbar-expand navbar-light bg-light',
                                'menu_class'        => 'navbar-nav d-flex justify-content-between w-100 second-main ',
                                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                                'walker'            => new wp_bootstrap_navwalker())
						); 
				?>
				
			

		</section>
    		
    		<!-- ##### res-menu ##### -->

	    <section id="res-menu">
	      <div>
		      <div class="white-div container-fluid py-4">
		        <p class="w-100 close-icon h4" onclick="closeMenu()">&times;</p>
			      <ul class="first-main-res">
			      </ul>
		      </div>


		      <!-- blue div -->
		      <div class="blue-div py-4">
			      <ul class="second-main-res">

			      </ul>
		      </div>
	      </div>
	    </section>