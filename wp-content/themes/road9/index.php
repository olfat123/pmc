<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Road9
 * @since 1.0.0
 */

get_header();
	               
		while ( have_posts() ) :
			// Start the Loop.
			
				the_post();
				?>
				<a href="<?php the_permalink(); ?>">
					<div class="blog-card" style="background-image: 'url(<?php the_post_thumbnail_url()?>)';"> 
						<div class="blog-card-body">							
								<div class="blog-card-body-date">							
									<p><?php echo get_the_date(); ?></p>
								</div>
								<div class="blog-card-body-footer">
									<p><?php the_title(); ?></p>
								</div>							
						</div>
					</div>
				</a>
				
				<?php
		endwhile;
		wp_reset_postdata();
		?>
		

<?php
core_paging_nav();

get_footer();
