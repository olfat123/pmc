﻿// ############ Preloader
var preloader;

function preload(opacity) {
    // back to <=0 before production
    if (opacity <= 0) {
        showContent();
    } else {
        preloader.style.opacity = opacity;
        window.setTimeout(function () {
            preload(opacity - 0.05)
        }, 100);
    };
};

function showContent() {
    preloader.style.display = 'none';
    document.getElementById('body-wrapper').style.visibility = 'visible';
    document.getElementById('body-wrapper').style.opacity = '1';
};

document.addEventListener("DOMContentLoaded", function () {
    preloader = document.getElementById('preloader');
    preload(1);
});
// ############# End of Preloader


// Menu
$(".first-main").clone().appendTo(".first-main-res");

$(".second-main").clone().appendTo(".second-main-res");
//menu overlay
//Toggle main menu open and close
function openMenu() {
    // document.getElementById('res-menu').style.display = 'block';
    document.getElementById('res-menu').style.visibility = 'visible';
    document.getElementById('res-menu').style.opacity = '1';
};

function closeMenu() {
    document.getElementById('res-menu').style.visibility = 'hidden';
    document.getElementById('res-menu').style.opacity = '0';
};

// Toggle responsive sub-menu open and close
let link = document.getElementsByClassName('res-dropdown-link');
let content = document.getElementsByClassName('res-dropdown-content');
let closeContent = document.getElementsByClassName('res-close');

for (let i = 0; i < link.length; i++) {
    //open the content related to the link
    link[i].addEventListener('click', show);

    function show() {
        content[i].style.visibility = 'visible';
        content[i].style.opacity = '1';
    };

    // Close content
    closeContent[i].addEventListener('click', hide);

    function hide() {
        content[i].style.visibility = 'hidden';
        content[i].style.opacity = '0';
    };
};


// Add arrow in case of small screen sizes

if ($(window).width() < 992) {
    $('<p class="text-light mx-5 mt-5 dropdown-title-text"></p>').prependTo('.dropdown-menu');
}



// Owl Carousel
 $('#owl-doctors').owlCarousel({
    loop:false,
    margin:16,
    rtl: true,
    dots:true,
    responsive:{
        0:{
            loop: true,
            items:1,
            stagePadding: 50
        },
        600:{
            items:2,
            stagePadding: 50
        },
        768:{
            items:3,
            stagePadding: 50
        },
        992:{
            items:3
        }
    }
})


  $('#owl-offers').owlCarousel({
    loop:false,
    margin:16,
    dots:true,
    rtl: true,
    responsive:{
      0:{
            loop: true,
            items:1,
            stagePadding: 50
        },
        600:{
            items:2,
            stagePadding: 50
        },
        768:{
            items:3,
            stagePadding: 50
        },
        992:{
            items:3
        }
    }
})

$('#owl-news').owlCarousel({
    loop:false,
    margin:16,
    dots:true,
    rtl: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1,
            stagePadding: 50
        },
        750:{
            items:2,
        },
        1000:{
            items:2
        }
    }
})

$('#owl-insurance').owlCarousel({
    loop:true,
    margin: 70,
    dots:false,
    nav: true,
    rtl: true,
    autoplay: true,
    smartSpeed: 3000,
    autoplayTimeout: 3000,
    autoplayHoverPause: false,
    responsive:{
      0:{
            margin: 30,
            items:3,
            stagePadding: 50
        },
        600:{
            margin: 30,
            items:4,
            stagePadding: 50
        },
        768:{
            margin: 30,
            items:5,
            stagePadding: 50
        },
        992:{
            margin: 30,
            items:7,
            stagePadding: 50
        }
    },
    navText : ["<div class='img-grey-prev'></div>","<div class='img-grey-next'></div>"]

})


$('#owl-doctors-specialties').owlCarousel({
    rtl: true,
	loop:false,
    margin:16,
    rtl: true,
	dots:true,
	responsive:{
			0:{
					loop: true,
					items:1,
					stagePadding: 50
			},
			600:{
					items:2,
			},
			768:{
					items:2,
			},
			992:{
					items:2,

			}
	}
});


// about-us page images animation

var animateHTML = function() {
    var elems;
    var windowHeight;
    function init() {
        elems = document.querySelectorAll('.img-cont');
        windowHeight = window.innerHeight;
        addEventHandlers();
        checkPosition();
    };
    function addEventHandlers() {
        window.addEventListener('scroll', checkPosition);
        window.addEventListener('resize', init);
    };
    function checkPosition() {
        for (var i = 0; i < elems.length; i++) {
            var positionFromTop = elems[i].getBoundingClientRect().top;
            if (positionFromTop - windowHeight <= 0) {
                elems[i].className = elems[i].className.replace(
                    'hidden',
                    'animate'
                );
            };
        };
    };
    return {
        init: init
    };
};
animateHTML().init();

// End of about-us page animation

// Tabs Medical/Beauty pages

$(document).ready(function(){
    $('#tabs-page .nav-link:first-of-type').addClass('active');
    var tab_id = $('#tabs-page .nav-link:first-of-type').attr('data-tab');
    $("#"+tab_id).addClass('active show fade');
})

$(document).ready(function(){
	$('#tabs-page .nav-link').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('#tabs-page .nav-link').removeClass('active');
		$('.tab-pane').removeClass('active');

		$(this).addClass('active');
        $("#"+tab_id).addClass('active show fade');
          });

        owl.init();
})




//  Prevent Tabs From Jumping
$(document).ready(function () {        

    $('#tabs-page .nav-link').click(function (evt) {
      // stops from submitting the form
      evt.preventDefault();
      return false;
  });


  $(".tab-pane").tabs();

  // This will work for dynamically added tabs as well

  $(".tab-pane").delegate('a', 'click', function(e){
       e.preventDefault();
       return false;
  });
});

// End of tabs for Medical/Beauty pages

// Careers page attach file button
$(document).ready(function(){
    $('input[type="file"]').addClass('fileToUpload');
    $('#careers label').addClass('attach');
    $('<div class="attach-img px-2 mt-2"></div>').prependTo('#careers label');
    $('#careers input[type="submit"]').wrap('<div class="btn text-uppercase"></div>');
    $('#careers .btn').append('<div class="img"></div>');
});

$(document).ready(function(){
    $('#book-appointment input[type="submit"]').wrap('<div class="btn text-uppercase"></div>');
    $('#book-appointment .btn').append('<div class="img"></div>');
});


$(document).ready(function(){
    $('#map input[type="submit"]').wrap('<div class="btn text-uppercase"></div>');
    var templateUrl = '<?= get_template_directory_uri(); ?>';
    $('#map .btn').append('<div class="img"></div>');
});

$(document).ready(function(){
    $('#myFooter .mc-field-group').append('<div class="img-blue"></div>');
});

// Careers page attach file button // End 

jQuery(function($) {
	$('#side-menu').on('change', function() {
		var url = $(this).val();
		if (url) {
			window.location = url;
		}
		return false;
	});
});


// Contact us alert
$(document).ready(function(){
    $("#alert-div").click(function () {
        $(this).addClass('fade');
    });
})

