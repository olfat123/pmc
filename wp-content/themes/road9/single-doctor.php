<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Road9
 * @since 1.0.0
 */

get_header();
$post_type = get_post_type();
?>
<div class="circle-sub" style="z-index: -1;"></div>
<div class="grey-section-full-page"></div>
<!-- after-header -->
    <div class="container-fluid p-5">
        <p class="text-uppercase page-sub-title"><?php echo __('Doctor','road9'); ?></p>
        <nav class="breadCrumbNav">
            <?php 
            breadcrumbs() ;
            ?>
        </nav>
    </div>
    <?php
        /* Start the Loop */
    while ( have_posts() ) :
        the_post();
        $speciality = get_post_meta( get_the_ID(), 'mb_speciality' , true );
            if($speciality){
                $speciality_name = get_the_title( $speciality );
                $icon = get_the_post_thumbnail_url($speciality);
            }else{
                $icon ="";
            }
            ?>
        <section id="doctors-single">
            <div class="container my-5">
                <div class="row">
                    <div class="card mb-3">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                            <img src="<?php the_post_thumbnail_url()?>;" class="card-img" alt="...">
                            <!-- <div class="card-img"></div> -->
                            </div>
                            <div class="col-md-8">
                                <div class="card-body p-0">
                                    <div class="cont">
                                    <?php the_title('<h6 class="h6">','</h6>'); ?>
                                        <p class="h7"><a href="<?php echo get_the_permalink($speciality); ?>"><img class="px-2" src="<?php echo $icon;?>"><?php echo $speciality_name; ?></a></p>
                                       
                                        <?php the_content(); ?>
                                    </div>
                                    <div class="clearfix"></div>
                                    <!-- ######### Book and appointment div -->
                                        <?php do_shortcode('[bookAppointment]'); ?>
                                      <!-- ######### Book and appointment div -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php

    endwhile; // End of the loop.
    ?>

<?php
get_footer();