<?php /* Template Name: Doctors */ 
get_header();
global $road9;
$button_icon = $road9['button-icon']['url'];
?>
<div class="circle-sub" style="z-index: -1"></div>
<div class="container-fluid p-5">
    <?php echo the_title(' <p class="text-uppercase page-sub-title">', '</p>'); ?>  
    <nav class="breadCrumbNav">
        <?php 
        breadcrumbs() ;
        ?>
    </nav>
</div>


<section id="doctors" class="doctors-page">
    <div class="container pb-5">            
        
        <div class="row">
            <?php
            $args = array(
                'post_type' => 'doctor',
                'posts_per_page' => -1,
                'orderby' => 'ID',
                'order' => 'ASC',                                
            );
            $query = new WP_Query( $args );
            while ( $query->have_posts() ) :
                $query->the_post();
                $speciality = get_post_meta( get_the_ID(), 'mb_speciality' , true );
                if($speciality){
                    $speciality_name = get_the_title( $speciality );
                }
                ?>
                <div class="col-lg-3 col-md-6 mb-4">
                    <div class="card text-white">
                    <?php if(has_post_thumbnail()){ ?>
                        <img class="card-img" src="<?php the_post_thumbnail_url() ; ?>" alt="Card image">
                        <?php  }else{ ?>

                            <img class="card-img" src="http://via.placeholder.com/360x640" alt="Card image">
                        <?php } ?>
                        <div class="card-img-overlay">
                            <a href="<?php the_permalink();?>" class="card-link text-uppercase">
                                <span class="card-link-text"><?php echo __( 'More about doctors', 'road9' ) ; ?></span>
                                <span class="card-link-arrow text-uppercase"><img src="<?php echo $button_icon; ?>" alt="arrow" class="px-2"></span>
                            </a>
                            <div class="card-text-overlay">
                                <?php the_title('<h5 class="card-title font-weight-bold">','</h5>'); ?>
                                <p class="card-text small"><a href="<?php echo get_the_permalink($speciality); ?>" ><?php echo $speciality_name; ?></a></p>
                            </div>
                        </div>
                    </div>
                </div>
                
            <?php
            endwhile; // End of the loop.
            wp_reset_postdata();
            ?>
        </div>
    </div>
</section>              

<?php
do_shortcode('[print_sponsers]');
get_footer();