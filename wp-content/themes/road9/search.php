<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Shape
 * @since Shape 1.0
 */

get_header(); ?>
<div class="circle-sub"></div>

  <div class="container-fluid p-5">
      <p class="text-uppercase page-sub-title"> Search for <?php echo get_search_query(); ?></p>
      <nav class="breadCrumbNav">
          <?php 
          breadcrumbs() ;
          ?>
      </nav>
  </div>
<?php
    global $query_string;
    $query_args = explode("&", $query_string);
    $search_query = array();

    foreach($query_args as $key => $string) {
      $query_split = explode("=", $string);
      $search_query[$query_split[0]] = urldecode($query_split[1]);
    } // foreach

    $the_query = new WP_Query($search_query);
    ?>
    <section id="search-page">
	    <div class="container">
		    <?php
		    if ( $the_query->have_posts() ) : 
		    ?>
		    <!-- the loop -->
		   
		    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

		    	<div class="search-card mb-3">
		            <div class="text-content">
			            <p class="title"><?php the_title(); ?></p>
			            <p class="small para"><?php the_excerpt(); ?></p>
			        </div>

		        	<div class="visit-page"><a href="<?php the_permalink(); ?>"><img src="<?php echo get_template_directory_uri().'/assets/img/arrow-small-right.png';?>" alt=""></a></div>
		        </div>
		          
		    <?php endwhile; ?>   
		    <!-- end of the loop -->
		    <?php wp_reset_postdata(); ?>

			<?php else : ?>
			    <p><?php _e( 'Sorry, no posts matched your criteria.','road9' ); ?></p>
			<?php endif; ?>
		</div>
	</section>
<?php get_footer(); ?>


    
     
    