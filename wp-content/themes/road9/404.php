<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Road9
 * @since 1.0.0
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

			<div class="error-404 not-found text-center">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Error 404 ', 'road9' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
                    
                    <h3><?php _e( 'It seems that page you are looking for no longer exists.','road9'); ?></h3>
                    <h3><?php _e( 'Please Go to <a href="'.home_url().'">homepage</a>', 'road9' ); ?></h3>
				</div><!-- .page-content -->
			</div><!-- .error-404 -->

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();