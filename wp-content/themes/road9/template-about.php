<?php /* Template Name: About */ 
get_header();
?>
<div class="circle-sub"></div>
<div class="grey-section-full-page"></div>
  <div class="container-fluid p-5">
      <?php echo the_title(' <p class="text-uppercase page-sub-title">', '</p>'); ?>  
      <nav class="breadCrumbNav">
          <?php 
          breadcrumbs() ;
          ?>
      </nav>
  </div>


  
        <?php 
        
        global $road9;
        $first_section_image = $road9['about-first-section-image']['url'];
        $second_section_image = $road9['about-second-section-image']['url'];
        $third_section_image = $road9['about-third-section-image']['url'];
        $last_section_image = $road9['last-section-image']['url'];

        if(get_locale()== 'en_GB'){
          $first_section_title = $road9['about-first-section-title'];
          $first_section_description = $road9['about-first-section-description'];

          $second_section_title = $road9['about-second-section-title'];
          $second_section_description = $road9['about-second-section-description'];

          $third_section_title = $road9['about-third-section-title'];
          $third_section_description = $road9['about-third-section-description'];

          $vision = $road9['company-vision'];
          $mission = $road9['company-mission'];

          $last_paragraph_title = $road9['last-paragraph-title'];
          $last_paragraph_description = $road9['last-paragraph-description'];
          
        }else{
          $first_section_title = $road9['about-first-section-title-arabic'];
          $first_section_description = $road9['about-first-section-description-arabic'];

          $second_section_title = $road9['about-second-section-title-arabic'];
          $second_section_description = $road9['about-second-section-description-arabic'];

          $third_section_title = $road9['about-third-section-title-arabic'];
          $third_section_description = $road9['about-third-section-description-arabic'];

          $vision = $road9['company-vision-arabic'];
          $mission = $road9['company-mission-arabic'];

          $last_paragraph_title = $road9['last-paragraph-title-arabic'];
          $last_paragraph_description = $road9['last-paragraph-description-arabic'];
        }
?>

<section id="about-us">

<section>
    <div class="cont py-5">
        <div class="card p-5">
            <h5 class="card-title text-uppercase"><?php echo $first_section_title; ?></h5>
            <p class="small para"><?php echo $first_section_description; ?></p>
        </div>
        <div class="img-cont hidden" style="background: url(<?php echo $first_section_image; ?>) no-repeat center center;"></div>
    </div>
</section>

<section>
    <div class="cont py-5">
        <div class="card p-5">
            <h5 class="card-title text-uppercase"><?php echo $second_section_title; ?></h5>
            <p class="small para"><?php echo $second_section_description; ?></p>
        </div>
        <div class="img-cont hidden" style="background: url(<?php echo $second_section_image; ?>) no-repeat center center;"></div>
    </div>
</section>

<section>
    <div class="cont py-5">
        <div class="card p-5">
            <h5 class="card-title text-uppercase"><?php echo $third_section_title; ?></h5>
            <p class="small para"><?php echo $third_section_description; ?></p>
        </div>
        <div class="img-cont hidden" style="background: url(<?php echo $third_section_image; ?>) no-repeat center center;"></div>
    </div>
</section>


<section id="about-us-hero" class="pt-5">
  <div class="banner-about">
    <div class="overlay-about">
      <div class="container p-5">
        <div class="row">
          <div class="col-lg-6">
            <h2 class="h2 font-weight-bold text-uppercase text-light"><?php echo __( 'Mission', 'road9' ) ; ?></h2>
            <p class="small para text-light w-75"><?php echo $mission; ?></p>

            <h2 class="h2 font-weight-bold text-uppercase text-light pt-5"><?php echo __( 'Vision', 'road9' ) ; ?></h2>
            <p class="small para text-light w-75"><?php echo $vision; ?></p>
          </div>
          <div class="col-lg-6">
          <h2 class="h2 font-weight-bold text-uppercase text-light pt-5"><?php echo $last_paragraph_title; ?></h2>
              <p class="small para text-light"><?php echo $last_paragraph_description; ?></p>
          </div>
        </div>
      </div>
      </div>
  </div>
</section>
  
<?php
get_footer();
?>