<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
######## sponsors ######## 

do_shortcode('[print_sponsers]');


######### End of Sponsors ########
?>

<div id="footer" style="width:100%"> 
  
      <footer id="myFooter">
          <div class="container pt-4">
              <div class="row">
                <div class="col-md-3">
                      <ul>
                          <p><?php echo __( 'Medical Services', 'road9' ) ; ?></p>
                          <?php
                                $args = array(
                                            'taxonomy' => 'medical_service_category',
                                            'hide_empty' => FALSE
                                        );

                                $cats = get_categories($args);

                                foreach($cats as $cat) {
                                ?>
                                    <a href="<?php echo get_category_link( $cat->term_id )?>">
                                    <li><?php echo $cat->name; ?></li>
                                    </a>
                                <?php
                                }
                            ?>
                          
                      </ul>
                      

                      <ul>
                          <p><?php echo __( 'Beauty Services', 'road9' ) ; ?></p>
                          <?php
                                $args = array(
                                            'taxonomy' => 'beauty_service_category',
                                            'hide_empty' => FALSE
                                        );

                                $cats = get_categories($args);

                                foreach($cats as $cat) {
                                ?>
                                    <a href="<?php echo get_category_link( $cat->term_id )?>">
                                    <li><?php echo $cat->name; ?></li>
                                    </a>
                                <?php
                                }
                            ?>
                          
                      </ul>

                </div>

                <div class="col-md-4">
                    <ul class="split-list">
                        <p><?php echo __( 'SPECIALTIES', 'road9' ) ; ?></p>
                    <?php
                       $args = array(
                            'post_type' => 'speciality',
                            'orderby' => 'ID',
                            'order' => 'ASC',                                
                        );
                        $query = new WP_Query( $args );
                        while ( $query->have_posts() ) :
                            $query->the_post();
                            ?>
                               <li><a href="<?php the_permalink();?>"> <?php the_title('', ''); ?></a></li>
                            <?php

                        endwhile;
                        ?>
                        
                    </ul>

                </div>
                  <div class="col-lg-5 col-md-9 col-sm-12 info">
                    <p class="pb-4 subscribe"><?php echo __( 'SUBSCRIBE FOR OUR NEWSLETTER', 'road9' ) ; ?></p>
                      <div class="form">
                        <!-- Begin Mailchimp Signup Form -->

<div id="mc_embed_signup">
<form action="https://road9media.us3.list-manage.com/subscribe/post?u=a5b38107cd80ba98cda84f17c&amp;id=8db323e99b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">


      <div class="mc-field-group">
        
        <input type="email" value="" placeholder="<?php echo __('Email','road9');?>" name="EMAIL" class="required email" id="mce-EMAIL">
        <input type="submit" value="<?php echo __('Subscribe','road9');?>" name="subscribe" id="mc-embedded-subscribe" class="button">
      </div>
      <div id="mce-responses" class="clear">
        <div class="response" id="mce-error-response" style="display:none;color: #21730f;"></div>
        <div class="response" id="mce-success-response" style="display:none;color: #d23518;"></div>
      </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
      <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_a5b38107cd80ba98cda84f17c_8db323e99b" tabindex="-1" value=""></div>
      
        
      
    </div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
                      </div>

                      <div class="social-icons pb-5">
                          <p class="pt-5 pb-2"><?php echo __( 'FOLLOW US', 'road9' ) ; ?></p>
                          <a href="<?php echo $facebook; ?>" class="px-2"><img src="<?php echo site_url(); ?>/wp-content/uploads/2019/06/facebook-logo.png" alt=""></a>
                          <a href="<?php echo $twitter ;?>" class="px-2"><img src="<?php echo site_url(); ?>/wp-content/uploads/2019/06/twitter.png" alt=""></a>
                          <a href="<?php echo $youtube ;?>" class="px-2"><img src="<?php echo site_url(); ?>/wp-content/uploads/2019/06/youtube.png" alt=""></a>
                          <a href="<?php echo $instagram ;?>" class="px-2"><img src="<?php echo site_url(); ?>/wp-content/uploads/2019/06/instagram.png" alt=""></a>
                      </div>

                  </div>
              </div>
            </div>
      </footer>
      <footer class="second-footer">
            <div class="container pt-4">
            <p class="copy-rights">© <?php echo date('Y'); ?> 
                <?php $blog_info = get_bloginfo( 'name' ); ?>
                <?php if ( ! empty( $blog_info ) ) : ?>
                    <a  href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"> <?php bloginfo( 'name' ); ?></a>, All Rights Reserved.
                <?php endif; ?>
                <a  href="<?php echo esc_url( __( 'https:/road9media.com/', 'road9' ) ); ?>" target="_blank">
                    <?php
                    printf( __( 'Website designed & developed by Road9 Media', 'road9' ), 'road9' );
                    ?>
                </a>    </p>

            <p class="terms">
                <a class="link" href="<?php echo get_permalink(pll_get_post(48));?>"><?php echo __( 'Disclaimer', 'road9' ) ; ?></a>
                <a class="link px-5" href="<?php echo get_permalink(pll_get_post(50));?>"><?php echo __( 'Terms & Conditions', 'road9' ) ; ?></a>
            </p>
            </div>
        </footer>
      
</div>
            

<?php wp_footer(); ?>

<script>
    AOS.init();
</script>

</body>
</html>
