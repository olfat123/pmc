��    0      �              >    R  \     �     �     �     �     �  
   �     �       	   	                    3     8     I     Q     X     ]     o     �     �  
   �  
   �     �     �     �     �  &   �     	     "	     6	     =	     P	     Y	     a	     p	     �	     �	  	   �	     �	     �	     �	     �	     �	     �	  �  �	  A  �  Z  #     ~     �     �     �     �     �     �                -     :     K  
   e     p  
   �     �     �     �      �     �                    &  5   7  
   m     x  9   �     �  !   �  
   �     �          (     9     X      u     �     �     �     �     �  
   �              <div class="prev px-3">
    <span>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.172 10.889">
        <defs>
          <style>
            .cls-1 {
              fill: #CECECE;
            }
          </style>
        </defs>
        <path id="right-arrow" class="cls-1" d="M13.114,4.838a.549.549,0,1,0-.78.773l3.957,3.957H-2.454A.544.544,0,0,0-3,10.115a.55.55,0,0,0,.546.554H16.291l-3.957,3.95a.56.56,0,0,0,0,.78.547.547,0,0,0,.78,0l4.894-4.894a.537.537,0,0,0,0-.773Z" transform="translate(3 -4.674)"/>
      </svg>
    </span>
    <p>Previous</p>
  </div> <div class="next px-3">
         
    <span>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.172 10.889">
          <defs>
            <style>
              .cls-2 {
                fill: #187EC1;
              }
            </style>
          </defs>
          <path id="right-arrow" class="cls-2" d="M13.114,4.838a.549.549,0,1,0-.78.773l3.957,3.957H-2.454A.544.544,0,0,0-3,10.115a.55.55,0,0,0,.546.554H16.291l-3.957,3.95a.56.56,0,0,0,0,.78.547.547,0,0,0,.78,0l4.894-4.894a.537.537,0,0,0,0-.773Z" transform="translate(3 -4.674)"/>
        </svg>
    </span><p>Next</p>
  </div> About Beauty Services Book Appointment Book an appointment Contact Disclaimer Doctors FAQ FOLLOW US Friday Home Insurance Companies Jobs Medical Services Mission Monday More More about doctor More about doctors News Our Services Our latest Promotions SPECIALTIES SUBSCRIBE FOR OUR NEWSLETTER Saturday Send Us A Message Sorry, no posts matched your criteria. Specialities Specialties Clinics Sunday Terms & Conditions Thursday Tuesday VIEW ALL NEWS  VIEW ALL PROMOTIONS View all doctors Vision Wednesday address application form booking form clinic phone working hours Project-Id-Version: Road9
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-07-02 10:14+0000
PO-Revision-Date: 2019-07-02 10:14+0000
Last-Translator: road9 <olfatrashad88@gmail.com>
Language-Team: Arabic
Language: ar
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100 >= 3 && n%100<=10 ? 3 : n%100 >= 11 && n%100<=99 ? 4 : 5;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.0; wp-5.2.2 <div class="prev px-3">
    <span>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.172 10.889">
        <defs>
          <style>
            .cls-1 {
              fill: #CECECE;
            }
          </style>
        </defs>
        <path id="right-arrow" class="cls-1" d="M13.114,4.838a.549.549,0,1,0-.78.773l3.957,3.957H-2.454A.544.544,0,0,0-3,10.115a.55.55,0,0,0,.546.554H16.291l-3.957,3.95a.56.56,0,0,0,0,.78.547.547,0,0,0,.78,0l4.894-4.894a.537.537,0,0,0,0-.773Z" transform="translate(3 -4.674)"/>
      </svg>
    </span>
    <p>السابق</p>
  </div> <div class="next px-3">
         
    <span>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.172 10.889">
          <defs>
            <style>
              .cls-2 {
                fill: #187EC1;
              }
            </style>
          </defs>
          <path id="right-arrow" class="cls-2" d="M13.114,4.838a.549.549,0,1,0-.78.773l3.957,3.957H-2.454A.544.544,0,0,0-3,10.115a.55.55,0,0,0,.546.554H16.291l-3.957,3.95a.56.56,0,0,0,0,.78.547.547,0,0,0,.78,0l4.894-4.894a.537.537,0,0,0,0-.773Z" transform="translate(3 -4.674)"/>
        </svg>
    </span><p>التالي</p>
  </div> عن خدمات التجميل إحجز ميعاد إحجز ميعاد اتصل بنا إخلاء المسئولية الأطباء الاسئلة الشائعة تابعنا الجمعه الرئيسية شركات التأمين وظائف الخدمات الطبية  الهدف الاثنين المزيد المزيد عن الطبيب المزيد عن الدكتور الأخبار خدماتنا أحر العروض التخصصات اشترك لتصلك نشرتنا الاخبارية السبت راسلنا معذرة . لا توجد نتائج لهذا البحث التخصصات العيادات المتخصصة الأحد الشروط والأحكام الخميس الثلاثاء عرض جميع الأخبار عرض جميع العروض اعرض جميع الأطباء الرؤية الأربعاء العنوان استمارة التقديم استمارة الحجز عيادة الهاتف مواعيد العمل 