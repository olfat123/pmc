<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
define( 'WP_AUTO_UPDATE_CORE', false );
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'pmc' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-1+j.H;G^_3<f4B*#<!(?Msx>q7^?c_dTC3Y^<Aj0Yviyd.4Z.Ipe}>70jwv*5&u' );
define( 'SECURE_AUTH_KEY',  'l9|R^,qKi+^lfuAOf~Hg^+@?0EB2W]<2v75d!f`ayJj4J!}~pWm0rC #R&DuMoot' );
define( 'LOGGED_IN_KEY',    '*=l/|3uZ)oCg*5uNGxrvtS#+twug< E+hldm[YK_s`hjWjV,zLQ(E OQtHGG<]B8' );
define( 'NONCE_KEY',        'jKH$!k9j5!6Eeww!B;bE;USvILVS6c}ufI)GayQ0%w>n3Z/rt,vs&$Lx>]ATI8(o' );
define( 'AUTH_SALT',        'H_d`G!:0-i|{k>o8EtiFhkO<baAR9aQz,q(2yH=x2}%z<A+f$th>IN<qfCJ#G5|>' );
define( 'SECURE_AUTH_SALT', 'I>ev{QGa& (VG:U=x,.FbuRQ4yMTVHDWRFOFd#BN?$tk:jT3aT/Jk:BM$YZ)lLH.' );
define( 'LOGGED_IN_SALT',   'U7sFnO9Qs=db}y9P+NtvOX&@%P$15Fm.:/Ar)+nHOK_h%NO.3>iO)<Qx;oSq1u,]' );
define( 'NONCE_SALT',       'S|vJQCLo|sylK}74v(30R6d~Cw.}kqG.|jsyg-@-0GcI!{EdA/:P|@.5hL]6C>.F' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'r9_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
